<?php

/**
 * Main configuration for the CCP
 */

return [
    'app_name' => 'CCP',
    'dashboard_route' => 'marketplace',
    'login_page' =>'auth/login',
    'base_url' => env('API_BASEURL', null)
];
<?php

/**
 * List of available APIs and Parameters and Default values
 */

return [
    // Auth Resource
    'auth' => [

        'GET' => [
            'oauth2' => [
                'end_point' => 'auth/oauth2',
                'params' => ['grant_type', 'username', 'password'],
                'cache' => true,
                'use_token' => false,
                'headers' => ['Accept' => 'text/plain'],
                'cacheFor' => 'user'
            ],

            'verifiedResetToken' => [
                'end_point' => 'auth/is-valid-token/{token}',
                'params' => ['token'],
                'cache' => false,
                'use_token' => false,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ]
        ],

        'POST' => [
            'oauth2' => [
                'end_point' => 'auth/oauth2',
                'params' => ['grant_type', 'username', 'password'],
                'use_token' => false,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'text/plain']
            ],

            'issueToken' => [
                'end_point' => 'auth/login',
                'params' => ['email', 'password', 'portal_type'],
                'cache' => false,
                'use_token' => false,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],

            'forgotPassword' => [
                'end_point' => 'auth/create-password-token',
                'params' => ['email', 'portal_type'],
                'cache' => false,
                'use_token' => false,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],

            'resetPassword' => [
                'end_point' => 'auth/reset-password',
                'params' => ['password', 'token'],
                'cache' => false,
                'use_token' => false,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ]

        ],

    ],

    'users-resource' => [

        'GET' => [
            'users' => [
                'end_point' => 'users/user',
                'params' => ['auth_token'],
                'cache' => true,
                'use_token' => true,
                'headers' => ['Accept' => 'text/plain'],
                'cacheFor' => 'user'
            ],
            'permissions' => [
                'end_point' => 'users/{userId}/permissions',
                'params' => ['userId'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Accept' => '*/*'],
                'cacheFor' => 'user'
            ]
        ]

    ],

    'account' => [

		'GET' => [
		    'validateEmail' => [
				'end_point' => 'account/validate-email/{token}/{emailToken}',
				'params' => ['token', 'emailToken', 'verificationCode'],
				'cache' => false,
				'use_token' => false,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],

			'getIndustryTpes' => [
//				'end_point' => 'account/get-industry-types',
				'end_point' => 'account/company-types',
				'params' => ['company_id', 'company_name', 'industry_type_id', 'company_size_id', 'email', 'portal_type'],
				'cache' => false,
				'use_token' => false,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],

			'getJobTitles' => [
//				'end_point' => 'account/get-job-titles',
				'end_point' => 'account/job-titles',
				'params' => ['portal_type'],
				'cache' => false,
				'use_token' => false,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],

			'getComapnySizes' => [
//				'end_point' => 'account/get-comapny-sizes',
				'end_point' => 'account/company-sizes',
				'params' => ['portal_type'],
				'cache' => false,
				'use_token' => false,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],

			'getSubscriptionPlans' => [
//				'end_point' => 'account/get-subscription-plans',
				'end_point' => 'account/subscription-plans',
				'params' => ['portal_type'],
				'cache' => false,
				'use_token' => false,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],

			'getLicenseAgreement' => [
//				'end_point' => 'account/get-license-agreement',
				'end_point' => 'account/license-agreement',
				'params' => ['portal_type'],
				'cache' => false,
				'use_token' => false,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],

			'getSettings' => [
				'end_point' => 'account/settings',
				'params' => [],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
            ],

            'getAddress' => [
				'end_point' => 'account/addresses',
				'params' => [],
				'cache' => false,
				'use_token' => true,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],
            'getAddressByType' => [
				'end_point' => 'account/addresses',
				'params' => ['company_id','type'],
				'cache' => false,
				'use_token' => true,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],


			'getStates' => [
				'end_point' => 'account/states',
				'params' => [],
				'cache' => false,
				'use_token' => true,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],

			'getDomains' => [
				'end_point' => 'account/domains',
				'params' => [],
				'cache' => false,
				'use_token' => true,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],
            'verifyInvite' => [
//                'end_point' => 'users/verify-invitation/{token}',
                'end_point' => 'users/invites/verify/{token}',
                'params' => ['token', 'portal_type'],
                'cache' => false,
                'use_token' => false,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],
		],

		'POST' => [

			'signupStep1' => [
				'end_point' => 'account/sign-up/step1',
				'params' => ['email', 'portal_type'],
				'cache' => false,
				'use_token' => false,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],

			'regenerateToken' => [
				'end_point' => 'account/regenerate-token',
				'params' => ['email'],
				'cache' => false,
				'use_token' => false,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],

			'validateEmail' => [
				'end_point' => 'account/validate-email',
				'params' => ['email_token', 'verification_code', 'portal_type'],
				'cache' => false,
				'use_token' => false,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],

			'addBusinessInformation' => [
//				'end_point' => 'account/add-business-information',
				'end_point' => 'account/business-information/create',
				'params' => ['company_id', 'email', 'company_name', 'company_type_id', 'company_size_id', 'portal_type'],
				'cache' => false,
				'use_token' => false,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],

			'addUserInformation' => [
//				'end_point' => 'account/add-user-information',
				'end_point' => 'account/user-information/create',
				'params' => ['email', 'first_name', 'last_name', 'phone', 'job_title_id', 'password', 'role_name', 'portal_type'],
				'cache' => false,
				'use_token' => false,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],

			'updateSubscriptionPlan' => [
//				'end_point' => 'account/update-subscription-plan',
				'end_point' => 'account/subscription-plans/{subscription_plan_id}/company/update',
				'params' => ['company_id', 'subscription_plan_id', 'email', 'portal_type'],
				'cache' => false,
				'use_token' => false,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],

			'updateCompanyAgreement' => [
//				'end_point' => 'account/update-company-agreement',
				'end_point' => 'account/license-agreement/{license_id}/company/update',
				'params' => ['company_id', 'license_id', 'email', 'portal_type'],
				'cache' => false,
				'use_token' => false,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],
			'updateSettings' => [
//				'end_point' => 'account/settings/change-settings',
				'end_point' => 'account/settings/change',
				'params' => ['key', 'value'],
				'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
			],

            'addressRequest' => [
                'end_point' => 'account/addresses',
                'params' => [],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],
            'addAddress' => [
                'end_point' => 'account/addresses/create',
                'params' => [],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],

            'updateAddress' => [
                'end_point' => 'account/addresses/change-address',
                'params' => [],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],

            'createDomain' => [
 				'end_point' => 'account/domains/create',
 				'params' => [],
 				'cache' => false,
 				'use_token' => true,
 				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],

            'updateRole' => [
				'end_point' => 'account/assign-role-user',
				'params' => ['userId', 'accountId', 'roleName'],
				'cache' => false,
				'use_token' => true,
				'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
			],
            'signupInvitedUser' => [
//                'end_point' => 'users/signup/invited-user',
                'end_point' => 'users/invites/signup',
                'params' => ['email', 'first_name', 'last_name', 'phone', 'job_title_id', 'password', 'role_name', 'portal_type'],
                'cache' => false,
                'use_token' => false,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],
            'createAddress' => [
//                'end_point' => 'account/create-address',
                'end_point' => 'account/address-of-use/create',
                'params' => ['organization_name', 'address_line_1', 'address_line_2', 'city', 'state', 'zip_code', 'phone', 'ext', 'type', 'account_id', 'email'],
                'cache' => false,
                'use_token' => false,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],
		],

        'DELETE'=>[

            'removeDomain' => [
                'end_point' => 'account/domains/{id}',
                'params' => ['id'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],
        ],
	],

    'user' => [

		'GET' => [
			'getUser' => [
				'end_point' => 'users',
				'params' => ['page', 'count', 'q', 'company_id'],
				'cache' => true,
				'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']

			],
            'getUserRequests' => [
//				'end_point' => 'users/requests',
				'end_point' => 'users/requests/company_id/{company_id}',
				'params' => ['page', 'count', 'q', 'company_id'],
				'cache' => true,
				'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']

			],
            'getUserInvitations' => [
				'end_point' => 'users/invites',
				'params' => ['page', 'limit', 'q'],
				'cache' => true,
				'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']

			],
            'getRole' => [ // NEED TO TEST
//				'end_point' => 'users/fetch-role-permissions/portal-type/{portal_type}',
				'end_point' => 'users/roles/portal-type/{portal_type}',
				'params' => ['portal_type'],
				'cache' => true,
				'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']

			],

            'getProfile' => [
                'end_point' => 'users/profile/{id}',
                'params' => ['id'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ]
		],

        'POST'=>[

            'userInvite' => [
                'end_point' => 'users/invites/create',
                'params' => ['email', 'role_name'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],
            'resendInvite' => [
                'end_point' => 'users/invites/resend',
                'params' => ['email', 'role_name'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],

            'updateUserRequest' => [
                'end_point' => 'users/requests/{id}/update',
                'params' => ['id', 'status'],
                'cache' => true,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],
            'changePassword' => [
                'end_point' => 'users/update-password',
                'params' => ['current_password', 'password', 'password_confirmation'],
                'cache' => true,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']

            ],

            'updateProfile' => [
                'end_point' => 'users/profile/{id}/update',
                'params' => ['id', 'first_name', 'last_name', 'job_title_id', 'phone', 'ext', 'mobile', 'status'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],

        ],

        'DELETE'=>[

            'cancelInvite' => [
                'end_point' => 'users/invites/{id}',
                'params' => ['id'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],
        ],

        'PATCH'=>[
            'updateUserProfile' => [
                'end_point' => 'users/profile/{id}/update',
                'params' => ['id', 'status', 'role_name'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],
        ],

	],

    'communication' => [

        'GET'=>[

            'getNotificationList' => [
                'end_point' => 'communications/notifications/account_id/{account_id}/user_id/{user_id}',
                'params' => ['account_id', 'user_id'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],
        ],

        'PATCH'=>[

            'updateNotification' => [
                'end_point' => 'communication/notifications/update',
                'params' => ['id', 'flagged', 'read'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],
        ],
    ],

    'payment' => [
        'GET' => [
            'getCreditCardDetail' => [
//                'end_point' => 'payments/credit-card',
                'end_point' => 'payments/credit-card/company_id/{company_id}',
                'params' => ['company_id'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],

            'getNetTermDetail' => [
//                'end_point' => 'payments/net-terms',
                'end_point' => 'payments/net-terms/company_id/{account_id}',
                'params' => ['account_id'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],
        ],
        'POST' => [
            'addCreditCard' => [
                'end_point' => 'payments/credit-card/create',
                'params' => ['name', 'number', 'cvv', 'month', 'type', 'year','company_id'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],

            'createNetTerms' => [
                'end_point' => 'payments/net-terms/create',
                'params' => ['organization', 'address', 'address2', 'city', 'state', 'zip', 'phone', 'type', 'duns_number', 'federal_id_number'],
                'cache' => false,
                'use_token' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ],
        ],


    ],
];

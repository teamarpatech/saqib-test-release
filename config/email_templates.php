<?php

$from_email = 'no-reply@ccp.com';

return [
	'forgot_password' => [
		'template_id' => '497797ec-2619-42bd-80d4-f574ecb8cfc7',
		'subject' => 'Connection Cloud Platform – Reset Password',
		'from_name' => 'Connection Cloud Platform Registration DO NOT REPLY',
		'from_email' => $from_email,
	],

	'email_address_verification' => [
		'template_id' => 'bc578367-2513-471b-92e0-229701a0ce91',
		'subject' => 'Connection Cloud Platform Registration - Email Verification Step',
		'from_name' => 'Connection Cloud Platform Registration DO NOT REPLY',
		'from_email' => $from_email,
	],

	'account_access_pending' => [
		'template_id' => 'ef4727ef-1751-4848-b830-d6490d45a4f2',
		'subject' => 'Connection Cloud Platform Registration – Account Access Pending',
		'from_name' => 'Connection Cloud Platform Registration DO NOT REPLY',
		'from_email' => $from_email,
	],
	'account_access_requested' => [
		'template_id' => '362512a3-96c8-4f6f-a807-eba15c5dfb15',
		'subject' => 'Connection Cloud Platform Registration – Account Access Requested',
		'from_name' => 'Connection Cloud Platform Registration DO NOT REPLY',
		'from_email' => $from_email,
	],

	'invited_user_email' => [
		'template_id' => '54fefe5f-6a59-4c2d-8fc9-08455ef391d7',
		'subject' => 'Connection Cloud Platform Registration – User Invite',
        'from_name' => 'Connection Cloud Platform Registration DO NOT REPLY',
		'from_email' => $from_email,
    ],

	'user_request_status_update_approved' => [
		'template_id' => '6f64dcc9-2ca2-456c-9ff2-090390695630',
		'subject' => 'Connection Cloud Platform – Account Access Approved',
		'from_name' => 'Connection Cloud Platform Acess DO NOT REPLY',
		'from_email' => $from_email,
	],

	'user_request_status_update_rejected' => [
		'template_id' => '0ce3e624-486e-4023-a380-ff7d81b2d868',
		'subject' => 'Connection Cloud Platform – Account Access Rejected',
		'from_name' => 'Connection Cloud Platform Acess DO NOT REPLY',
		'from_email' => $from_email,
	],

	'successful_user_registration_with_new_company' => [
		'template_id' => 'fd51f79f-4cc9-4c31-a138-bb1f8da4e800',
		'subject' => 'Connection Cloud Platform – New Account Created',
		'from_name' => 'Connection Cloud Platform DO NOT REPLY',
		'from_email' => $from_email,
	]
];
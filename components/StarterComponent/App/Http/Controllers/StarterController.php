<?php

namespace Component\StarterComponent\App\Http\Controllers;

use App\Http\Controllers\Controller;

class StarterController extends Controller {

 
  public function __construct() {
    
  }

  /**
  * Requests Instagram pics.
  *
  * @return Response
  */
  public static function index()
  {
    return view('StarterComponent::index');
  }
}
import '../../../vue_common/vue-config';
import '../../../vue_common/vue-validation';
import '../../../vue_common/vue-mixin';

//vue object
var AccountSettings = new Vue({
    el: "#AccountSettings",
    data: {
        root_url: root_url,
        showMessage: false,
        messageClass: '',
        messageText: '',
        allow_more_accounts: _allow_more_accounts,
        disableButton: false,
        dataChanged: false,
    },
    methods: {
        // disable button
        disable: function () {
            this.disableButton = true;
        },
        // enable button
        enable: function () {
            this.disableButton = false;
        },
        // change
        onChange: function () {
            if((this.allow_more_accounts === true && _allow_more_accounts === 0) ||  (this.allow_more_accounts === false && _allow_more_accounts === 1)){
                this.dataChanged = true;
            }
        },
        // reset change
        resetChange: function () {
            this.showMessage = false;
            this.dataChanged = false;
        },
        // update account settings
        updateSettings: function () {
            this.disable();
            let settings = {
                'key': 'allow_more_accounts',
                'value': ((this.allow_more_accounts===true)?1:0),
            };
            this.$http.post('account/settings', settings).then(response => response.json()).then(
                (response) => {
                    if (parseInt(response.data.error_type) === 1) {
                        this.dataChanged = false;
                        this.handleMessage(true, response.data.error_type, 'Account settings updated successfully.');
                    }
                    else
                        this.handleMessage(true, response.data.error_type, response.message);
                    this.enable();
                }, (error) => {
                    this.enable();
                    console.error('account settings : post error');
                    console.error(error);
                },
            );
        },
        // revert object
        onCancel: function () {
            if(this.dataChanged === true) {
                this.allow_more_accounts = ((_allow_more_accounts===1)?true:false);
            }
            this.resetChange();
        },
    }
});
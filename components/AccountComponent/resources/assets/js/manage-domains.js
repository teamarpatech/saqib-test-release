import '../../../vue_common/vue-config';
import '../../../vue_common/vue-validation';
import '../../../vue_common/vue-mixin';
import '../../../vue_common/vue-directive';

Vue.component('confirmation-popup', require('../components/confirmation-popup.vue'));

let domainApp = new Vue({
    el : '#domainApp',
    data : {

        domainList: "",
        addNewForm: false,

        removedIndex: -1,

        // button field
        addDomainButton : true,

        //input field
        domain: '',

        // show messages
        showMessage : false,
        messageClass : '',
        messageText : '',

        root_url : root_url
    },

    mounted() {
        this.getDomainList();
    },

    methods:{

        showAddNewForm : function () {
            this.showMessage = false;
            this.addNewForm = true;
            this.addDomainButton = false;
        },

        hideAddNewForm : function () {
            this.addNewForm = false;
            this.domain = '';
            this.showMessage = false;
            this.addDomainButton = true;
        },

        // click submit button
        formSubmit : function () {
            this.validateBeforeSubmit();
        },

        // get all existing domains
        getDomainList : function () {
            this.$http.post('account/get-domains', []).then(response => response.json()).then(
            (response) => {
                if (response.success && response.status_code == 200) {
                    this.domainList = response.data.domains;
                }
            }, (error) => {
                console.error('account/get-domains');
                console.error(error);
            });
        },

        // send request to controller
        createDomain : function () {

            let postData = {
                'domain' : this.domain
            };

            this.$http.post('account/create-domain', postData).then(response => response.json()).then(
            (response) => {

                if (response.success && response.status_code == 200) {

                    this.domain = '';
                    this.getDomainList();
                    this.hideAddNewForm();
                }

                this.handleMessage(true, response.data.error_type, response.message);

            }, (error) => {
                console.error('account/create-domain');
                console.error(error);
            });
        },

        // validate form
        validateBeforeSubmit() {
            this.$validator.validateAll().then((result) => {

                if (result) {
                    this.createDomain();
                    this.messageText = '';
                    this.messageClass = '';
                    this.showMessage = false;
                    return;
                }

                this.handleMessage(true, 0, this.errorBag.all()[0]);
            });
        },

        removeDomainAlert : function(index) {
            this.showMessage = false;
            this.removedIndex = index;
            window.EventBus.$emit('putValueInConfirmationPopup', {domainList : this.domainList, removedIndex : this.removedIndex})
        }
    },
});
import '../../../vue_common/vue-config';
import '../../../vue_common/vue-validation';
import '../../../vue_common/vue-directive';

Vue.component('request-address-form', require('../components/request-address-form.vue'));

new Vue({
    el : '#billingApp',
    data : {

        shipping_data : {
            type : "address of use",
            heading : "Address of Use",
            request_button_value : "Request Address of Use Change",
        },

		root_url : root_url
    },
});

@extends('layouts.new_master_shop')

@push('styles')
    <link href="{{ mix('components/account/css/manage-domains.css') }}" rel="stylesheet">
@endpush

@section('page_title', 'Manage Domains')
@section('content_shop')

    <section id="domainApp" class="container my-profile clearfix">
        <!-- Trigger the modal with a button -->
        <!-- Modal -->

        <confirmation-popup></confirmation-popup>

        @include('partials.account.left_side_bar')



        <!-- Main Starts Here -->
        <section id="billingApp" class="main-content container">


            <!-- Manage Domains Starts Here -->
            <div v-cloak class="account-settings-cont">


                <div class="main-billing">
                    <!-- Page Heading Starts Here -->
                    <div class="page-heading">
                        <span>Manage Domains</span>
                    </div>
                    <!-- Page Heading Ends Here -->

                    <!-- Domain Main Container Starts Here -->
                    <div class="domain-container">
                        <!-- Domain heading and Sorting Starts Here -->
                        <div class="domain-header col-sm-12">
                            <span class="domain-heading">Domain Name</span>
                            <span class="domain-sort hide"></span>
                        </div>
                        <!-- Domain heading and Sorting Ends Here -->

                        <div class="clearfix"></div>

                        <!-- Domains List Starts Here -->
                        <div class="domain-list">
                            <div class="col-lg-12">
                                <div class="row">
                                    <ul>

                                        <li v-for="(row, index) in domainList">
                                            <div class="row">
                                                <div class="domain pull-left col-sm-9 col-xs-9" v-text="row.domain"></div>

                                                <div class="remove-domain pull-right col-sm-3 col-xs-3" v-if="row.is_default == 1">
                                                    <span>Default</span>
                                                </div>

                                                <div class="remove-domain pull-right col-sm-3 col-xs-3" v-if="row.is_default == 0">
                                                    <a href="#" data-toggle="modal" @click="removeDomainAlert(index)" data-target="#myModal">
                                                        Remove
                                                    </a>
                                                </div>
                                            </div>
                                        </li>

                                        <!-- Domains Ends Here -->
                                    </ul>
                                </div>
                            </div>


                            <div class="clearfix"></div>
                        </div>

                        <!-- Domains List Ends Here -->


                        <div class="domain-add col-sm-4">
                            <div>

                                <div class="col-sm-12 col-xs-12" v-if="addDomainButton">
                                    <div class="form-group ">
                                        <button @click="showAddNewForm()" class="btn-primary-lg" >Add Domain</button>
                                    </div>
                                </div>


                                <!-- Add New Domain Feilds Starts Here -->

                                <div class="domain-add-fields" v-if="addNewForm">

                                    <div class="col-sm-12 col-xs-12">
                                        <div class="form-group row">
                                            <input v-model="domain" v-focus type="text" class="form-control ccp-txtbox" v-validate="'required'" data-vv-as="Domain">
                                        </div>
                                    </div>


                                    <div class="col-sm-12 col-xs-12">
                                        <div class="form-group row">
                                            <div class="col-sm-6 col-xs-6" style="padding-left: 0">
                                                <button @click="formSubmit()" class="btn-primary-lg">Add</button>
                                            </div>
                                            <div class="col-sm-6 col-xs-6" style="padding-right: 0">
                                                <button class="btn-user-bordered-lg" @click="hideAddNewForm()">Cancel</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!-- Add New Domain Feilds Ends Here -->

                                <div class="clearfix"></div>

                            </div>
                        </div>

                        <!-- Success and other messages starts here -->
                        <div class="col-sm-12" v-if="showMessage">
                            <div class="row">
                                <alert :root_url="root_url" :message_class="messageClass" :message_text="messageText" :show_message="showMessage"></alert>
                            </div>
                        </div>

                        <!-- Success and other messages Ends here -->


                    </div>
                    <!-- Domain Main Container Ends Here -->


                </div>
                <!-- Manage Domains Ends Here -->


            </div>


        </section>
        <!-- Main Ends Here -->

    </section>
@endsection


@push('scripts')
    <script type="text/javascript" src="{{ mix('components/account/js/manage-domains.js') }}"></script>
@endpush


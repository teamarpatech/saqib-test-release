@extends('layouts.new_master_shop')
@push('styles')
    <link href="{{ mix('components/account/css/settings.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.css">
@endpush
@section('page_title', 'Account Settings')
@section('content_shop')
    <section class="container my-profile clearfix">
        @include('partials.account.left_side_bar')
        <!-- Users Listing Starts Here -->
        <section id="AccountSettings" class="main-content container">
            <!-- Account Settings Starts Here -->
            <div class="account-settings-cont">
                <!-- Page Heading Starts Here -->
                <div class="page-heading">
                    <span>Account Settings</span>
                </div>
                <div class="light-para">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud
                    </p>
                </div>
                <!-- Page Heading Ends Here -->
                <!-- Settings Starts Here -->
                <div class="setting-form">
                    <div class="UserAccountChk">
                        <div class="col-xs-12 row">
                            <div class="col-md-10 col-sm-10 col-xs-12">

                             <span class="checkbox checkbox-primary">
                                <input id="active2" v-model="allow_more_accounts" @change="onChange" value="1" name="active" type="checkbox" class="custom-control-input">
                                <label for="active2"></label>
                            </span>

                            <span class="ccp-label">
                                Allow More Accounts
                            </span>


                            <div class="light-para">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod tempor incididunt ut labore et.
                                    Ut enim ad minim veniam, quis nostrud </p>
                            </div>

                            </div>
                            {{--<div class="col-md-2 col-sm-2 col-xs-3">--}}
                           {{----}}
                            {{--</div>--}}
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-xs-12 row">
                            <div class="col-md-10 col-sm-10 col-xs-12">

                             <span class="checkbox checkbox-primary">
                                <input id="active1" name="active" type="checkbox" class="custom-control-input">
                                <label for="active1"></label>
                            </span>

                            <span class="ccp-label">
                                Allow Self Enrollments
                            </span>


                                <div class="light-para">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et.
                                        Ut enim ad minim veniam, quis nostrud </p>
                                </div>

                            </div>
                            {{--<div class="col-md-2 col-sm-2 col-xs-3">--}}
                           {{----}}
                            {{--</div>--}}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <!-- File Upload Starts Here -->
                    <div class="logoUpload clearfix">
                        <div class="col-md-12 col-sm-12 col-xs-12 row">
                            <div class="col-lg-6 col-md-5 col-sm-5 col-xs-12">
                            <span class="ccp-label">
                                Upload Company Logo
                            </span>
                            </div>
                            <div class="col-lg-5 col-md-7 col-sm-12 col-xs-10">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <span class="btn btn-default btn-file">
                                        <span>Browse</span>
                                        <input type="file" multiple/>
                                    </span>
                                    <div class="filemsgs">
                                        <span class="fileinput-filename"></span>
                                        <span class="fileinput-new">Logo file must be in .png format and must not be any bigger than 5 MB.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- File Upload Ends Here -->
                    <!-- Action Buttons Starts Here -->
                    <div class="settings-btn">
                        <alert :root_url="root_url" :show_message="showMessage" :message_class="messageClass" :message_text="messageText"></alert>
                        <div class="col-md-12 col-sm-12 row account-settings-btn">
                            <div class="col-sm-6 col-xs-6">
                                <button class="btn-primary-lg" :disabled="disableButton" @click="updateSettings">Save Changes</button>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <button @click="onCancel" class="btn-user-bordered-lg">Cancel</button>
                            </div>
                        </div>
                    </div>
                    <!-- Action Buttons Ends Here -->
                </div>
                <!-- Settings Ends Here -->
            </div>
            <!-- Account Settings Ends Here -->
        </section>
        <!-- Users Listing Ends Here -->
    </section>
@endsection
@push('scripts')
    <script>
        let _allow_more_accounts = parseInt({{$allow_more_accounts}});
        let current_url = "{{URL::current()}}";
    </script>
    <script type="text/javascript" src="{{ mix('components/account/js/settings.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.js"></script>
@endpush

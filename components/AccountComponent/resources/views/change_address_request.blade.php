@extends('layouts.new_master_shop')

@push('styles')
    <link href="{{ mix('components/account/css/address-request.css') }}" rel="stylesheet">
@endpush

@section('page_title', 'Address of Use')

@section('content_shop')

    <section class="container my-profile clearfix">

        @include('partials.account.left_side_bar')

        <!-- Billing & Details Starts Here -->
        <section id="billingApp" class="main-content container">

            <!-- Billing Address Starts Here -->
            <div class="account-settings-cont">

                <div class="main-UseAddress">
                    <request-address-form :root_url="root_url" :form_data="shipping_data"></request-address-form>
                </div>

            </div>
            <!-- Billing Address Ends Here -->

        </section>
        <!-- Billing & Details Ends Here -->

    </section>

@endsection

@push('scripts')
    <script type="text/javascript" src="{{ mix('components/account/js/address-request.js') }}"></script>
@endpush



<?php

Route::group(['prefix' => 'account'], function () {
    Route::group([
        'middleware' => 'userpermission:address-management',
    ], function () {
        Route::get('address-of-use', 'ChangeAddressRequestController@addressRequest')->name('address-request');
        Route::post('post-address-request', 'ChangeAddressRequestController@postAddressRequest')->name('post-address-request');

        Route::post('get-states', 'ChangeAddressRequestController@getStates')->name('get-states');
        Route::post('get-address', 'ChangeAddressRequestController@getAddress')->name('get-address');
    });

    Route::group([
        'middleware' => 'userpermission:settings-management',
    ], function () {
        Route::get('settings', 'AccountController@settings')->name('account-settings');
        Route::post('settings', 'AccountController@updateSettings');
    });

    Route::group([
        'middleware' => 'userpermission:domain-management',
    ], function () {
        Route::get('manage-domains', 'ManageDomainsController@index')->name('manage-domains');
        Route::post('get-domains', 'ManageDomainsController@getDomains')->name('get-domains');
        Route::post('create-domain', 'ManageDomainsController@createDomain')->name('create-domain');
        Route::post('remove-domain', 'ManageDomainsController@removeDomain')->name('remove-domain');
    });
});
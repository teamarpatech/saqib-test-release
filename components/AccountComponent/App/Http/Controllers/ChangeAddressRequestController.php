<?php

    namespace Component\AccountComponent\App\Http\Controllers;

    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Session;

    class ChangeAddressRequestController extends Controller
    {
        // change address form
        public function addressRequest()
        {
            return view('AccountComponent::change_address_request');
        }

        // send adress change request to manager
        public function postAddressRequest(Request $request) {

            $params = $request->only('organization_name', 'address_line_1', 'address_line_2', 'city', 'state', 'zip_code', 'phone', 'type', 'ext', 'address_id', 'create_or_update');
            $params['account_id'] = \AuthHandler::company_id();

            if($params['create_or_update'] == 'create') {
                $result['message'] = "You don't have permission to add address please contact to admin.";
                $result['status_code'] = "401";
                $response = response()->json($result);
                //$response = filterResponse(handleResponse(GetAPI('account', 'POST', 'addAddress', $params)));
            }
            else {
                $response = filterResponse(handleResponse(GetAPI('account', 'POST', 'updateAddress', $params)));
            }

            return $response;
        }

        // Fetch existing address
        function getAddress() {
            $params['type'] = 'address of use';
            $params['account_id'] = \AuthHandler::company_id();

            return filterResponse(handleResponse(GetAPI('account', 'GET', 'getAddress', $params)));
        }

        // Fetch all states from API server.
        function getStates() {
            return filterResponse(handleResponse(GetAPI('account', 'GET', 'getStates', [])));
        }
    }
    
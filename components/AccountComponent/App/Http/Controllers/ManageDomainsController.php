<?php

namespace Component\AccountComponent\App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageDomainsController extends Controller
{
    // domain listing page
    public function index() {

        $domains = $this->getDomains();
        return view('AccountComponent::manage-domains', compact('domains'));
    }

    // domain all domains from api
    function getDomains() {
        return filterResponse(handleResponse(GetAPI('account', 'GET', 'getDomains', [])));
    }

    // create new domain
    function createDomain(Request $request) {
        $params = $request->only('domain');
        return filterResponse(handleResponse(GetAPI('account', 'POST', 'createDomain', $params)));
    }

    // create new domain
    function removeDomain(Request $request) {
        $params = $request->only('id');
        return filterResponse(handleResponse(GetAPI('account', 'DELETE', 'removeDomain', $params)));
    }
}
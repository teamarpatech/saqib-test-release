<?php

namespace Component\AccountComponent\App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{

    public function settings()
    {
        $data = filter_response(handleResponse(GetAPI('account', 'GET', 'getSettings', [])));
        $settings_data = [
            'allow_more_accounts' => array_get_value_by_key_value($data['data'], 'key', 'allow_more_accounts')
        ];
        return view('AccountComponent::settings', $settings_data);
    }

    public function updateSettings(Request $request)
    {
        return filter_response(handleResponse(GetAPI('account', 'POST', 'updateSettings', $request->only(['key', 'value']))));
    }

}
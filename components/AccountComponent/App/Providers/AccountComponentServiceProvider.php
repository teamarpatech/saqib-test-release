<?php

namespace Component\AccountComponent\App\Providers;

use Illuminate\Support\ServiceProvider;
use Route;

class AccountComponentServiceProvider extends ServiceProvider
{

    protected $ComponentName;
    protected $path;

    public function __construct($app)
    {
        parent::__construct($app);

        $this->name = 'Account';
        $this->ComponentName = $this->name.'Component';
        $this->path = __DIR__ . '/../../';
    }

   /**
    * Bootstrap the application services.
    *
    * @return void
    */
   public function boot()
   {
       $this->loadViewsFrom($this->path.'/resources/views', $this->ComponentName);

        $this->publishes([
            $this->path . 'resources/assets' => resource_path('assets/components/' . strtolower($this->name)),
        ], $this->ComponentName);
   }

    /**
     * Register the application services.
     * @return void
     */
    public function register()
    {
        //Register Our Package routes
        Route::group([
            'namespace' => 'Component\\' . $this->ComponentName . '\App\Http\Controllers',
            'as' => $this->ComponentName . '.',
            'middleware' => ['web']
        ], function ($router) {
            include $this->path . 'routes/routes.php';
        });

        // Component local configuration
        $this->mergeConfigFrom(
            $this->path . 'config/' . $this->ComponentName . '.php', $this->ComponentName
        );
        //register helpers
        $this->registerHelpers();
    }

    /**
     * Register helpers file
     */
    public function registerHelpers()
    {
        // Load the helpers in app/Http/helpers.php
        if (file_exists($helpers = $this->path . 'App/Helpers/helpers.php'))
            require_once $helpers;

    }

}
<?php
/**
 * filtering response by encoding
 */
if (! function_exists('array_search_key_value')) {
    function array_get_value_by_key_value($array = [], $key = '', $value = '') {
        if(!is_array($array) || $key == '' || count($array) === 0)
            return -1;
        $filtered = array_first(array_where($array, function ($_value, $_key) use ($key, $value) {
            if($_value[$key] == $value)
                return $_value;
        }))['value'];
        if($filtered === NULL)
            return -1;
        else
            return $filtered;
    }
}
import '../../../vue_common/vue-config';
import '../../../vue_common/vue-validation';
import '../../../vue_common/vue-mixin';
import '../../../vue_common/vue-directive';
let changePassword = new Vue({
    el : '#changePassword',
    data : {
        current_password : '',
        password : '',
        password_confirmation : '',
        // show messages
        showMessage : false,
        messageClass : '',
        messageText : '',
        submitButtonEnabled : true,
        root_url : root_url
    },
    mounted() {
    },
    methods:{
        formSubmit : function() {
            this.validateBeforeSubmit();
        },
        clearFields : function() {
            this.current_password = '';
            this.password = '';
            this.password_confirmation = '';
            this.showMessage = false;
        },
        changePassword : function () {
            let postData = {
                "current_password" : this.current_password,
                "password" : this.password,
                "password_confirmation" : this.password_confirmation
            };
            this.$http.post('users/post-change-password', postData).then(response => response.json()).then(
            (response) => {
                this.messageClass = 0;
                if (response.success && response.status_code == 200) {
                    this.submitButtonEnabled = false;
                    this.clearFields();
                    this.messageClass = 1;
                    setInterval(() => {
                        this.logout();
                    }, 1000);
                }
                this.handleMessage(true, this.messageClass, response.message);
            }, (error) => {
                console.error('users/post-change-password');
                console.error(error);
            });
        },
        validateBeforeSubmit() {
                this.$validator.validateAll().then((result) => {
                if (result) {
                    this.changePassword();
                    this.messageText = '';
                    this.messageClass = '';
                    this.showMessage = false;
                    return;
                }
                this.handleMessage(true, 0, this.errorBag.all()[0]);
            });
        },
        logout : function () {
            let postData = {};
            this.$http.post('users/logout', postData).then(response => response.json()).then(
            (response) => {
                if (response.status_code == 200) {
                    window.location.href = this.root_url+"/auth/login";
                }
            }, (error) => {
                console.error('users/logout');
                console.error(error);
            });
        },
    }
});
import '../../../vue_common/vue-config';
import '../../../vue_common/vue-validation';
import '../../../vue_common/vue-pagination';
import '../../../vue_common/vue-filter';
import '../../../vue_common/vue-mixin';

Vue.component('user-listing', require('../components/userListing.vue'));
Vue.component('user-invite', require('../components/userInvite.vue'));
Vue.component('user-request', require('../components/userRequest.vue'));
Vue.component('send-user-invite', require('../components/sendUserInvite.vue'));

var UsersApp = new Vue({
    el:'#UsersApp',
    data() {
        return {
            root_url: root_url,
            search_keyword: '',
            roles: [],
            tabValue: 'user',
            total_user_count: '',
            total_invite_count:'',
            total_request_count:'',
        }
    },
    mounted(){
        //searching on load
        window.EventBus.$emit('globalSearch', {
            search_keyword: this.search_keyword,
        });
        this.tabValue='user';
        //listen userRoles event
        window.EventBus.$on('userRoles', (data) => {
            this.roles = data.roles;
        });
        //listen totalRecordsUser event
        window.EventBus.$on('totalRecordsUser', (data) => {
            this.total_user_count = '('+data.record+')';
        });
        //listen totalRecordsInvite event
        window.EventBus.$on('totalRecordsInvite', (data) => {
            this.total_invite_count = '('+data.record+')';
        });
        //listen totalRecordsRequest event
        window.EventBus.$on('totalRecordsRequest', (data) => {
            this.total_request_count = '('+data.record+')';
        });
        //listen updateUserListing event
        window.EventBus.$on('updateUserListing',(data) => {
            //searching on load
            window.EventBus.$emit('userListing', {
                search_keyword: this.search_keyword,
            });
        });
    },
    methods: {

        //listen click events on blade template and set value for current tab
        ClickUserTab() {
            this.tabValue='user';
        },
        ClickInviteTab() {
            this.tabValue='invite';
        } ,
        ClickRequestTab() {
            this.tabValue='request';
        },
        //searching on enter press
        submit() {
            let self = this;
          //reset value  previous search value on submit
            this.total_user_count='';
            this.total_invite_count='';
            this.total_request_count='';
            window.EventBus.$emit('globalSearch', {
                search_keyword: this.search_keyword,
            });
        }
    }
});
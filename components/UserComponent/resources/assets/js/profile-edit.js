import '../../../vue_common/vue-config';
import '../../../vue_common/vue-validation';
import '../../../vue_common/vue-mixin';

let profileEdit = new Vue({
    el : '#profileEdit',
    data : {

        // text
        name_initials : '',

        //input fields
        first_name : '',
        last_name : '',
        job_title_id : '',
        phone : '',
        ext : '',
        mobile : '',
        email : '',

        // show messages
        showMessage : false,
        messageClass : '',
        messageText : '',

        root_url : root_url,

        // dropdown
        jobTitleDropDown:''
    },

    mounted() {
        this.getProfile();
        this.getJobTitle();
    },

    methods:{

        nameInitials : function () {

            this.name_initials = ''

            if(this.first_name != '')
                this.name_initials += this.first_name.charAt(0).toUpperCase();

            if(this.last_name != '')
                this.name_initials += this.last_name.charAt(0).toUpperCase();
        },

        cancelSubmit : function () {
            this.redirectToProfileViewPage();
        },

        formSubmit : function () {
            this.validateBeforeSubmit();
        },

        // get existing address
        getProfile : function() {
            this.$http.post('users/profile-fetch', []).then(response => response.json()).then(
                (response) => {
                    if (response.success && response.status_code == 200) {
                        this.first_name = response.data.first_name;
                        this.last_name = response.data.last_name;
                        this.job_title_id = response.data.job_title_id;
                        this.phone = response.data.phone;
                        this.email = response.data.email;
                        this.ext = '';
                        this.mobile = '';
                        if(response.data.profile != null) {
                            this.ext = response.data.profile.ext;
                            this.mobile = response.data.profile.mobile;
                        }
                        this.nameInitials();
                    }
                }, (error) => {
                    console.error('users/profile-fetch');
                    console.error(error);
                }
            );
        },

        // get existing address
        getJobTitle : function() {

            this.$http.post('users/get-job-title', []).then(response => response.json()).then(
                (response) => {
                    if (response.success && response.status_code == 200) {
                        this.jobTitleDropDown = response.data;
                    }
                }, (error) => {
                    console.error('users/get-job-title');
                    console.error(error);
                }
            );
        },

        // validate form
        validateBeforeSubmit() {

            this.$validator.validateAll().then((result) => {

                if (result) {
                    this.profileUpdate();
                    this.messageText = '';
                    this.messageClass = '';
                    this.showMessage = false;
                    return;
                }

                this.handleMessage(true, 0, this.errorBag.all()[0]);
            });
        },

        redirectToProfileViewPage : function () {
            window.location.href = this.root_url+"/users/my-profile";
        },

        profileUpdate : function() {

            let postData = {
                'first_name' : this.first_name,
                'last_name' : this.last_name,
                'job_title_id' : this.job_title_id,
                'phone' : this.phone,
                'ext' : this.ext,
                'mobile' : this.mobile,
            };

            this.$http.post('users/profile-update', postData).then(response => response.json()).then(
                    (response) => {

                        this.messageClass = 0;
                        if (response.success && response.status_code == 200) {
                            this.messageClass = 1;
                            this.redirectToProfileViewPage();
                        }

                        this.handleMessage(true, this.messageClass, response.message);

            }, (error) => {
                    console.error('users/profile-update');
                    console.error(error);
                },
            );
        },
    },
});
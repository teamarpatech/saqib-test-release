@extends('layouts.new_master_shop')
@push('styles')
    <link href="{{ asset('components/user/css/users.css') }}" rel="stylesheet">
@endpush
@section('page_title', 'User Management')
@section('content_shop')
    <section class="container my-profile clearfix">
    @include('partials.account.left_side_bar')
        <!-- Users Listing Starts Here -->
        <section id="UsersApp" class="main-content container">


            <div class="users-search-access pull-left">
                <div class="page-md-heading pull-left "><span>User Management</span></div>
            </div>


        <div class="pull-right search-user">
        <input type="text" placeholder="Search" v-model=search_keyword @keyup.enter="submit" class="ccp-txtbox">

                </div>

               <div class="clearfix"></div>
            <!-- User Listing Page Starts Here -->
                <ul class="nav nav-tabs ">
                    <li :class="[tabValue=='user' ? 'active' : '']" @click="ClickUserTab"><a data-toggle="tab" href="#allUsers">Users <span v-html="total_user_count"></span></a></li>
                    <li :class="[tabValue=='invite' ? 'active' : '']"  @click="ClickInviteTab"><a data-toggle="tab" href="#invites">Invites <span v-html="total_invite_count"></span></a></li>
                    <li :class="[tabValue=='request' ? 'active' : '']" @click="ClickRequestTab"><a data-toggle="tab" href="#RequestUsers">Requests <span v-html="total_request_count"></span></a></li>
                </ul>
                <div class="tab-content">
                    <!-- List Of Users Starts Here -->
                    <user-listing :root_url="root_url" ></user-listing>
                    <!-- List Of Users Ends Here -->
                    <user-invite :root_url="root_url" :roles="roles"></user-invite>
                    <!-- Users Request List Starts Here -->
                    <user-request :root_url="root_url" ></user-request>
                    <!-- Users Request List Starts Here -->
                </div>
            <!-- User Listing Page Ends Here -->
        </section>
        <!-- Users Listing Ends Here -->
    </section>
@endsection
@push('scripts')
    <script>
        window.userManagement = {!! json_encode([
                    'prefix'     => Route::current()->getPrefix(),

                ]) !!};
    </script>
    <script src="{{ asset('components/user/js/users.js') }}"></script>
@endpush



@extends('layouts.master_shop')
@section('page_title', 'My Profile')
@section('content_shop')

    <section class="container my-profile clearfix">

        @include('partials.account.left_side_bar')

        <section class="main-content container">
            <h1>My Profile</h1>
            <div class="user-container clearfix">
                <div class="left">
                    <div class="user-photo">
                        {{strtoupper($profile['first_name'][0].$profile['last_name'][0])}}
                    </div>
                </div>
                <div class="right">
                    <h2>
                        {{$profile['first_name']. " " .$profile['last_name']}}
                    </h2>
                    <ul>
                        <li>
                            {{$profile['job_title']['title']}}
                        </li>
                        <li>
                            {{$profile['phone']}}
                            @if($profile['profile']['ext'] != "")
                                , ext. {{$profile['profile']['ext']}}
                            @endif
                        </li>
                        <li>
                            {{$profile['email']}}
                        </li>
                    </ul>
                    <a href="{{route('UserComponent.profile-edit')}}" class="btn edit">Edit Profile</a>
                </div>
            </div>
            <div class="user-details-container clearfix">
                <div class="left">
                </div>
                <div class="right">
                    <div class="info-container">
                        <div class="info-section">
                            <ul>

                                @if( ! empty(\AuthHandler::company()->name))
                                <li>
                                    <label>Company:</label>
                                    <p>{{\AuthHandler::company()->name}}</p>
                                </li>
                                @endif
                                @if( ! empty(\AuthHandler::company()->industry_type->title))
                                <li>
                                    <label>Industry Type:</label>
                                    <p>{{\AuthHandler::company()->industry_type->title}}</p>
                                </li>
                                @endif
                                @if( ! empty(\AuthHandler::company()->company_size->title))
                                <li>
                                    <label>Number of Employees:</label>
                                    <p>{{\AuthHandler::company()->company_size->title}}</p>
                                </li>
                                @endif
                                @if( ! empty(\AuthHandler::company()->addresses))
                                <li>
                                    <label>Address:</label>
                                    <p>{{\AuthHandler::company()->addresses[0]->address_line_1}},{{\AuthHandler::company()->addresses[0]->city}},{{\AuthHandler::company()->addresses[0]->state->name}},{{\AuthHandler::company()->addresses[0]->zip_code}}</p>
                                </li>
                                @endif
                                @if( ! empty(\AuthHandler::phone()))
                                <li>
                                    <label>Phone:</label>
                                    <p>{{\AuthHandler::phone()}}</p>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ mix('components/user/js/profile-view.js') }}"></script>
@endpush
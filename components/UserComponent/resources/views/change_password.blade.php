@extends('layouts.master_shop')

@push('styles')

    <link href="{{ mix('components/user/css/change-password.css') }}" rel="stylesheet">


@endpush

@section('page_title', 'Change Password')
@section('content_shop')

    <section class="container my-profile clearfix">
        @include('partials.account.left_side_bar')
        <!-- Main Starts Here -->
        <section id="changePassword" class="main-content container">

            <!-- Change Pass Starts Here -->
                <div class="account-settings-cont">
                   <div class="main-billing">
                        <!-- Page Heading Starts Here -->
                         <div class="page-heading">
                             <span>Change Password</span>
                         </div>
                        <!-- Page Heading Ends Here -->

                        <!--Change Pass Fields Starts Here -->
                           <div class="ccp-form-row clearfix">
                               <div class="col-sm-12">

                                   <!-- Current Password Starts Here -->
                                   <div class="form-group row current-pass">

                                       <div class="col-lg-4 col-sm-4 col-xs-12 pr-0">
                                           <label for="currentPassword" class="form-label">Current Password</label>
                                       </div>

                                       <div class="col-lg-8 col-sm-8 col-xs-12">
                                           <input type="password" id="current_password" v-focus v-model="current_password" name="current_password" class="form-control ccp-txtbox ml-1" v-validate="'required'" data-vv-as="Current Password" />
                                       </div>

                                   </div>
                                   <!-- Current Password Ends Here -->
                                        <hr class="pass-seperator" />
                                   <!-- New Password Starts Here -->
                                   <div class="form-group row">

                                       <div class="col-lg-4 col-sm-4 col-xs-12 pr-0">
                                            <label for="password" class="form-label">New Password</label>
                                       </div>

                                       <div class="col-lg-8 col-sm-8 col-xs-12">
                                            <input type="password" id="password" v-model="password" name="password" class="form-control ccp-txtbox ml-1" v-validate="'required'" data-vv-as="New Password" />
                                       </div>

                                   </div>
                                   <!-- New Password Ends Here -->

                                   <!-- Confirm Password Starts Here -->
                                   <div class="form-group row">

                                       <div class="col-lg-4 col-sm-4 col-xs-12 pr-0">
                                           <label for="password_confirmation" class="form-label">Confirm Password</label>
                                       </div>

                                       <div class="col-lg-8 col-sm-8 col-xs-12">
                                           <input type="password" id="password_confirmation" v-model="password_confirmation" name="password_confirmation" class="form-control ccp-txtbox ml-1" v-validate="'required'" data-vv-as="Confirm Password" />
                                       </div>

                                   </div>
                                   <!-- Confirm Password Ends Here -->



                               </div>

                           </div>
                        <!--Change Pass Fields Ends Here -->

                       <alert :root_url="root_url" :message_class="messageClass" :message_text="messageText" :show_message="showMessage"></alert>

                        <!-- Pass Action Buttons Starts Here -->
                           <div class="ccp-form-row clearfix ">
                               <div class="col-sm-12">
                                   <div class="form-group row invite-form-btns">
                                       <div class="col-sm-6 col-xs-6">
                                           <button class="btn-primary-lg" @click="formSubmit()" :disabled="!submitButtonEnabled">Save Changes</button>
                                       </div>
                                       <div class="col-sm-6 col-xs-6">
                                           <button class="btn-user-bordered-lg ml-1" @click="clearFields()">Cancel</button>
                                       </div>
                                   </div>
                               </div>
                           </div>

                        <!-- Pass Action Buttons Ends Here -->
                    </div>
                </div>
            <!-- Change Pass Ends Here -->

        </section>
        <!-- Main Ends Here -->
    </section>

@endsection


@push('scripts')
    <script type="text/javascript" src="{{ mix('components/user/js/change-password.js') }}"></script>
@endpush



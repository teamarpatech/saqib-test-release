@extends('layouts.master_shop')

@push('styles')
    <link href="{{ mix('components/user/css/profile-edit.css') }}" rel="stylesheet">
@endpush


@section('page_title', 'Edit Profile')
@section('content_shop')
    <section class="container my-profile clearfix" id="profileEdit">
        @include('partials.account.left_side_bar')
        <section class="main-content container edit">
            <h1>Edit Profile</h1>
            <div class="user-container clearfix">
                <div class="left">
                    <div class="user-photo" v-text="name_initials"></div>
                </div>
            </div>
            <div class="user-details-container clearfix">
                <div class="left">
                </div>
                <div class="right">
                    <div class="info-container">
                        <form onsubmit="return false;">
                            <div class="info-section input-fields">
                                <div class="row">
                                    <div class="small-12 large-3 columns">
                                        <label title="first-name">First Name: <span class="asterik">*</span></label>
                                    </div>
                                    <div class="small-12 large-9 columns">
                                        <input title="first-name" @keyup="nameInitials" value="" v-model="first_name" v-validate="'required'" data-vv-name="First Name" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="small-12 large-3 columns">
                                        <label title="last-name">Last Name: <span class="asterik">*</span></label>
                                    </div>
                                    <div class="small-12 large-9 columns">
                                        <input title="last-name" @keyup="nameInitials()" value="" v-model="last_name" v-validate="'required'" data-vv-name="Last Name" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="small-12 large-3 columns">
                                        <label title="phone">Phone: <span class="asterik">*</span></label>
                                    </div>
                                    <div class="small-12 large-9 columns">
                                        <input title="phone" value="" v-model="phone" v-validate="'required|cPhone'" data-vv-name="Phone" v-mask="'###.###.####'" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="small-12 large-3 columns">
                                        <label title="extension">Ext.</label>
                                    </div>
                                    <div class="small-12 large-9 columns">
                                        <input title="extension" value="" v-model="ext" v-validate="'numeric'" data-vv-name="Ext." v-mask="'####'" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="small-12 large-3 columns">
                                        <label title="mobile-number">Mobile:</label>
                                    </div>
                                    <div class="small-12 large-9 columns">
                                        <input title="mobile-number" value="" v-model="mobile" v-validate="'cPhone'" data-vv-name="Mobile" v-mask="'###.###.####'" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="small-12 large-3 columns">
                                        <label title="title">Job Title: <span class="asterik">*</span></label>
                                    </div>
                                    <div class="small-12 large-9 columns">
                                        <select class="small native-drop native-drop-large" v-model="job_title_id" v-validate="'required'" data-vv-name="Job Title">
                                            <option value="">Select an Option</option>
                                            <option v-for="row in jobTitleDropDown" :value="row.id" v-text="row.title"></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="small-12 large-3 columns">
                                        <label title="email">Email:</label>
                                    </div>
                                    <div class="small-12 large-9 columns">
                                        <input title="email" value="" v-model="email" disabled/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 notification" v-if="showMessage">
                                <div class="row">
                                    <alert :root_url="root_url" :message_class="messageClass" :message_text="messageText" :show_message="showMessage"></alert>
                                </div>
                            </div>

                            <a href="javascript:void(0);" @click="formSubmit()" class="btn btn-primary">Save</a>
                            <a href="{{route('UserComponent.profile-view')}}" class="btn">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </section>

@endsection

@push('scripts')
    <script type="text/javascript" src="{{ mix('components/user/js/profile-edit.js') }}"></script>
@endpush
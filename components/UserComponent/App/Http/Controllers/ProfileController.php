<?php

    namespace Component\UserComponent\App\Http\Controllers;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use AuthHandler;
    use Illuminate\Support\Facades\Session;

    class ProfileController extends Controller
    {
        //user detail
        public function view()
        {
            $response = $this->getProfile();
            $profile  = $response['data'];

            return view("UserComponent::profile_view", compact('profile'));
        }

        //profile edit form
        public function edit(Request $request)
        {
            return view("UserComponent::profile_edit");
        }

        // fetch user profile from API server.
        public function getProfile()
        {
            $param['id'] = Session::get('users-resource')['id'];

            return filterResponse(handleResponse(GetAPI('user', 'GET', 'getProfile', $param)));
        }

        // Fetch all job titles from API server
        public function getJobTitles()
        {
            $param['portal_type'] = Session::get('users-resource')['portal_type'];

            return filterResponse(handleResponse(GetAPI('account', 'GET', 'getJobTitles', $param)));
        }

        // Update user profile on API Server.
        public function update(Request $request)
        {
            $params           = $request->only('first_name', 'last_name', 'job_title_id', 'phone', 'ext', 'mobile', 'status');
            $params['status'] = 1;
            $params['id']     = Session::get('users-resource')['id'];

            $this->updateSession($params);

            return $response = filterResponse(handleResponse(GetAPI('user', 'POST', 'updateProfile', $params)));

            $result['message'] = $response['message'];
            $result['success'] = $response['success'];
            if ($response['status_code'] <> 200 && is_array($response['message']))
            {

                foreach ($response['message'] as $row)
                {

                    $result['message'] = $row;
                    if (is_array($row))
                    {
                        $result['message'] = $row[0];
                    }
                    break;
                }
            }
            $result['status_code'] = $response['status_code'];

            return response()->json($result);
        }

        public function updateSession($params) {

            $updateSessionData['first_name'] = $params['first_name'];
            $updateSessionData['last_name'] = $params['last_name'];

            AuthHandler::infoUpdateInSession($updateSessionData);
        }
    }
<?php

namespace Component\UserComponent\App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class UsersRequestsController extends Controller
{

    /**
     * get all users requests array
     * @return array
     */
    public function getUsersRequests(Request $request)
    {
        $param = $request->only(['page', 'count', 'q']);
        $param['company_id'] = \AuthHandler::company_id();
        return filterResponse(handleResponse(GetAPI('user', 'GET', 'getUserRequests', $param)));
    }

    /**
     * update user request
     * @return array
     */
    public function updateUserRequest(Request $request)
    {
        $data = filterResponse(handleResponse(GetAPI('user', 'POST', 'updateUserRequest', ['id' => $request->id, 'status' => $request->status])));
        $data['email_sent'] = $this->sendRequestStatusEmail($request);
        return $data;
    }

    /**
     * email to user which request get accepted or rejected
     * @param Request $request
     * @param         $data
     * @return bool
     */
    public function sendRequestStatusEmail(Request $request)
    {
        /**
         * User Request Status Email
         */
        $params['template_data']                    = [];
        $params['template_data']['userName']        = $request->user['first_name'] . ' ' . $request->user['last_name'];
        $params['template_data']['adminName']       = session()->get('users-resource.first_name').' '.session()->get('users-resource.last_name');
        $params['template_data']['adminEmail']      = session()->get('users-resource.email');
        $params['template_data']['companyName']     = $request->user['company']['name']; //Input::get('user.company.name')
        // Account Access Approved Email
        if(intval($request->status) === 1){
            $params['template_data']['loginLink']   = url("/auth/login?email=".$request->user['email']);
            $config_key                             = 'user_request_status_update_approved';
        }
        // Account Access Rejected Email
        else if(intval($request->status) === 0){
            $config_key                             = 'user_request_status_update_rejected';
        }
        $params['to']['name']                       = $request->user['first_name'] . ' ' . $request->user['last_name'];
        $params['to']['email']                      = $request->user['email'];
        $params['from']['name']                     = config('email_templates.' . $config_key . '.from_name');
        $params['from']['email']                    = config('email_templates.' . $config_key . '.from_email');
        $params['subject']['title']                 = config('email_templates.' . $config_key . '.subject');
        $params['template']['id']                   = config('email_templates.' . $config_key . '.template_id');
        try{
            $emailResponse                          = sendEmailWithTemplate($params, "-", "-");
            if ($emailResponse === true) {
                return true;
            }
            return false;
        }catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

    public function replace_to_with($data, $to, $with){
        if(is_array($data) && count($data) > 0){
            foreach($data as $key => $value){
                $data[$key] = str_replace($to, $with, $value);
            }
        }
        return $data;
    }

}
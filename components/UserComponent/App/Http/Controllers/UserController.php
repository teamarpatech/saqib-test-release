<?php

namespace Component\UserComponent\App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    /**
     * show view of listing of users
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userListing()
    {
        return view('UserComponent::users_listing');
    }

    /**
    * get all users
    * @return array
    */
    public function getUser(Request $request)
    {
        $param = $request->only(['page', 'count', 'q']);
        $param['company_id'] = \AuthHandler::company_id();
        $response = [];
        $response['users'] = filter_response(handleResponse(GetAPI('user', 'GET', 'getUser', $param)));
        $response['roles'] = $this->getRoles($request);
        $response['logged_in_user_id'] = \AuthHandler::id();
        return $response;
    }

    /**
    * get all invited users
    * @return array
    */
    public function getUserInvitations(Request $request)
    {
        $param = $request->only(['page', 'limit','q']);
        return filter_response(handleResponse(GetAPI('user', 'GET', 'getUserInvitations', $param)));
    }

    /**
    * get all roles
    * @return array
    */
    public function getRoles(Request $request)
    {
        return filter_response(handleResponse(GetAPI('user', 'GET', 'getRole', ['portal_type' => $request->portal_type])));
    }

    /**
     * update roles based on user id
     * @return array
     */
    public function updateUserprofille(Request $request)
    {
        $param = $request->only(['id', 'status', 'role_name']);
        return filter_response(handleResponse(GetAPI('user', 'PATCH', 'updateUserProfile', $param)));
    }

    /**
     * invite user
     * @return array
     */
    public function userInvite(Request $request)
    {
        $param = $request->only(['email', 'role_name']);
        $response = filter_response(handleResponse(GetAPI('user', 'POST', 'userInvite', $param)));
        if (optional(optional($response)['data'])['error_type'] === 1)
            $response['email_sent'] = $this->sendInviteEmail($response);
        return $response;
    }

    /**
     * resend invite to user
     * @return array
     */
    public function resendInvite(Request $request)
    {
        $param = $request->only(['email', 'role_name']);
        $response = filter_response(handleResponse(GetAPI('user', 'POST', 'resendInvite', $param)));
        if (optional(optional($response)['data'])['error_type'] === 1)
            $response['email_sent'] = $this->sendInviteEmail($response);
        return $response;
    }

    /**
     * cancel invite to user
     * @return array
     */
    public function cancelInvite($id)
    {

        $param['id'] = $id;
        $response = filter_response(handleResponse(GetAPI('user', 'DELETE', 'cancelInvite', $param)));
        return $response;
    }

    /**
     * send email to invited user
     */
    public function sendInviteEmail($data)
    {
        $invite                                     = optional($data)['data'];
        $params['to']['name']                       = '';
        $params['to']['email']                      = $invite['invited']['email'];
        $params['from']['name']                     = config('email_templates.invited_user_email.from_name');
        $params['from']['email']                    = config('email_templates.invited_user_email.from_email');
        $params['subject']['title']                 = config('email_templates.invited_user_email.subject');
        $params['template']['id']                   = config('email_templates.invited_user_email.template_id');
        $params['template_data']                    = [];
        $params['template_data']['token']           = url('verify-invite/'.$invite['invited']['token']);
        $params['template_data']['adminName']       = $invite['invitee']['first_name'] . ' ' . $invite['invitee']['last_name'];
        $params['template_data']['companyName']     = $invite['invitee']['company'];
        try{
            $emailResponse                          = sendEmailWithTemplate($params, '-', '-');
            if ($emailResponse === true) {
                return true;
            }
            return false;
        }catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

}

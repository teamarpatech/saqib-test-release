<?php

namespace Component\UserComponent\App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use AuthHandler;

class ChangePasswordController extends Controller
{
    // change password form
    public function changePassword()
    {
        return view('UserComponent::change_password');
    }

    // password update on API Server.
    public function postChangePassword(Request $request) {

        $params = $request->only('current_password', 'password', 'password_confirmation');
        return $response = filterResponse(handleResponse(GetAPI('user', 'POST', 'changePassword', $params)));
    }

    // user logout after password changed.
    function logout(Request $request) {

        AuthHandler::logout($request);
        $result['status_code'] = 200;
        return response()->json($result);
    }
}
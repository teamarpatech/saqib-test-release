<?php

    Route::group([  'prefix' => 'user-management','middleware' => 'userpermission:user-management',], function () {

        //Routes related to user listing tabs
        Route::get('/', 'UserController@userListing')->name('userListing');
        Route::get('get-user-list', 'UserController@getUser')->name('getUser');
        Route::get('get-user-invitations', 'UserController@getUserInvitations')->name('getUserInvitations');
        Route::post('update-role', 'UserController@updateRole')->name('updateRole');
        Route::patch('update-user-profile', 'UserController@updateUserprofille')->name('updateUserprofile');
        Route::post('invite', 'UserController@userInvite')->name('userInvite');
        Route::post('resend-invite', 'UserController@resendInvite')->name('resendInvite');
        Route::delete('cancel-invite/{id}', 'UserController@cancelInvite')->name('cancelInvite');
        Route::post('get-users-requests', 'UsersRequestsController@getUsersRequests')->name('getUsersRequests');
        Route::post('update-user-request', 'UsersRequestsController@updateUserRequest')->name('updateUserRequest');
    });

    Route::group(['prefix' => 'users', 'middleware' => 'userpermission:password-management',], function () {

        Route::get('change-password', 'ChangePasswordController@changePassword')->name("change-password");
        Route::post('post-change-password', 'ChangePasswordController@postChangePassword')->name("post-change-password");
        Route::post('logout', 'ChangePasswordController@logout')->name("logout");
    });

    Route::group(['prefix' => 'users','middleware' => 'userpermission:profile-management',], function () {

        Route::get('my-profile', 'ProfileController@view')->name("profile-view");
        Route::get('edit-profile', 'ProfileController@edit')->name("profile-edit");
        Route::post('profile-update', 'ProfileController@update')->name("profile-update");
        Route::post('profile-fetch', 'ProfileController@getProfile')->name("profile-fetch");
        Route::post('get-job-title', 'ProfileController@getJobTitles')->name("get-job-title");
    });


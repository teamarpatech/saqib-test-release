<?php

namespace Component\LoginComponent\App\Providers;

use Illuminate\Support\ServiceProvider;
use Route;

class LoginComponentServiceProvider extends ServiceProvider {


    protected $ComponentName;
    protected $path;

    public function __construct($app)
    {
        parent::__construct($app);

        $this->ComponentName = 'LoginComponent';
        $this->path = __DIR__ . '/../../';
    }

   /**
    * Bootstrap the application services.
    *
    * @return void
    */
   public function boot()
   {
       $this->loadViewsFrom($this->path.'/resources/views', $this->ComponentName);
   }

   /**
    * Register the application services.
    *
    * @return void
    */
   public function register()
   {
       //Register Our Package routes
      Route::group([
          'namespace' => 'Component\\'.$this->ComponentName.'\App\Http\Controllers',
          'as' => $this->ComponentName.'.',
          'middleware'=>['web']
      ], function ($router) {
          include $this->path.'routes/routes.php';
      });

      // Component local configuration
      $this->mergeConfigFrom($this->path.'config/'.$this->ComponentName.'.php', $this->ComponentName);
  }

}
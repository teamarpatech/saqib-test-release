<?php

namespace Component\LoginComponent\App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Validator;
use Illuminate\Http\Request;
use APIHandler;
use AuthHandler;


class LoginController extends Controller
{
    // GET /auth/authenticate
    public function authenticate(Request $request)
    {
        return AuthHandler::login($request);
    }

    // GET /auth/logout
    public function logout(Request $request)
    {

        return AuthHandler::logout($request);
    }

    /**
     * GET login form
     * @return Login view with input fileds name and password
     */
    public function login(Request $request)
    {
        return view('LoginComponent::login');
    }

    /**
     * ForgotPassword
     * @return ForgotPassword view with input fiels
     **/
	public function forgotPassword(Request $request){
        $email = $request->get('email');
        return view('LoginComponent::forgot_password', compact('email'));
    }

    /**
     * Function call on calling login button
     * @param email
     * @param password
     * @return user authentication token
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, ['email' => 'email|required', 'password' => 'required', 'portal_type' => 'required']);

        // Check username and password from API and get token
        $response = handleResponse(GetAPI('auth','POST' ,'issueToken',$request->only('email','password', 'portal_type')));

        // checking repsonse from api if api is not returning success it will redirect to back window with message
        if($response['response']->success != true){
            $request->session()->flash('api_messages', $response['response']->message);

            if(array_key_exists('render', $response['response']->data)) {
                $request->session()->flash('render', $response['response']->data->render);
            }

            return redirect()->back()->withInput();
        }

        // api returning access token for next steps after login
        $authToken = $response['response']->data->access_token;
        // redirecting to this authenticate method for calling user API

        $url = "auth/authenticate?auth_token={$authToken}";
        $url = request()->has('redirect_to')? $url.'&redirect_to='.request()->input('redirect_to') : $url;
        return redirect($url);
    }

    /** ForgetPassword
     *function for send link via email to user for password forgot request
     */
	public function forgotPasswordPost(Request $request) {

        $request->validate(['email' => 'required|email'], ['email' => 'Email address is invalid. Kindly provide a valid email address.']);

        // call api to get token for user verification
        $result = GetAPI('auth', 'POST', 'forgotPassword', $request->only('email', 'portal_type'));

        // https request handling
        $response = handleResponse($result);

        // if api server returns error
        if($response['response']->status_code != 200 || !$response['response']->success) {
            $errorMessage = $response['response']->message;
            return view('LoginComponent::forgot_password', compact('errorMessage'));
        }

        // Send verification link to user via email
        $params['to']['name'] = '';
        $params['to']['email'] = $request->get('email');

        $params['from']['name'] = config('email_templates.forgot_password.from_name');
        $params['from']['email'] = config('email_templates.forgot_password.from_email');

        $params['subject']['title'] = config('email_templates.forgot_password.subject');

        $params['template']['id'] = config('email_templates.forgot_password.template_id');
        $params['template_data']['password_reset_link'] = url('auth/reset-password/'.$response['response']->data->token);

        $emailResponse = sendEmailWithTemplate($params);

        if($emailResponse !== true) {
            $errorMessage = "Email Not Sent Please Try Again.";
            return view('LoginComponent::forgot_password', compact('errorMessage'));
        }

        $successMessage = $response['response']->message;
        return view('LoginComponent::forgot_password', compact('successMessage'));
    }

    /**
     *   resetPassword
     *@return  ResetPassword view with input fiels
     */
	public function resetPassword($token)
    {
        // call api to verify url token
        $result = GetAPI('auth', 'GET', 'verifiedResetToken', ['token' => $token]);

        // https request handling
        $response = handleResponse($result);

        if(!$response['response']->success) {
            $errorMessage = $response['response']->message;
            $errorMessageApi = true;
            return view('LoginComponent::reset_password', compact( 'errorMessage', 'token', 'errorMessageApi'));
        }

        return view('LoginComponent::reset_password')->with('token', $token);
    }

    /** resetPasswordPost
     *validate token and update password.
     */
	public function resetPasswordPost(Request $request)
    {
        // password validation muust be 8 characters including number, uppercase , lowercase
        $request->validate(['password' => ['required','min:8','regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/','confirmed']]);
        $token = $request->get('token');
        // call api to verify url token
        $result = GetAPI('auth', 'GET', 'verifiedResetToken', $request->only('token'));

        // https request handling
        $response = handleResponse($result);

        if(!$response['response']->success) {
            $errorMessage = $response['response']->message;
            return view('LoginComponent::reset_password', compact( 'errorMessage','token'));
        }

        // token verified now update password
        $result = GetAPI('auth', 'POST', 'resetPassword', $request->only('token', 'password', 'password_confirmation'));

        // https request handling
        $response = handleResponse($result);

        if(!$response['response']->success) {
            foreach($response['response']->message as $value) {
                $errorMessage = $value[0];
            }
            return view('LoginComponent::reset_password', compact('errorMessage', 'token'));
        }

        return redirect(url('auth/login'))->with('successMessage', $response['response']->message);
    }
}

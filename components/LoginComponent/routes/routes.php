<?php


Route::group(['middleware'=>'guest','prefix' => 'auth'],function(){
    Route::get('login','LoginController@login')->name('login');
    Route::post('login','LoginController@postLogin')->name('postLogin');
    Route::get('forgot-password', 'LoginController@forgotPassword')->name('forgotPassword');
    Route::post('forgot-password', 'LoginController@forgotPasswordPost')->name('forgotPasswordPost');
    Route::get('reset-password/{token}', 'LoginController@resetPassword')->name('resetPassword');
    Route::post('reset-password/{token}', 'LoginController@resetPasswordPost')->name('resetPasswordPost');
});


Route::group(['prefix' => 'auth'], function () {
    Route::get('logout', 'LoginController@logout')->name('logout');
    Route::get('authenticate', 'LoginController@authenticate')->name('authenticateUser');
});





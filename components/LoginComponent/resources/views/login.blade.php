<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>CCP Login</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('login_assets/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('login_assets/css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('login_assets/css/mediaqurries.css')}}" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>
</head>
<body>
<div class="container-fluid main-container h-sm-auto">
    <div class="row justify-content-center h-100 h-sm-auto">
        <div class="col-sm-12 p-0 h-sm-auto">
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 hidden-md-down login-form-cont float-right float-sm-left h-sm-auto pt-5 pb-5"> <!-- col-sm-6 col-md-10 col-lg-8 col-xl-8 -->
                <div class="h-100">
                    <div class="row align-items-center h-100 h-sm-auto">
                        <!-- Form Container Starts Here -->
                        <div id="container" class="container">
                            <div class="row">
                                <div class="m-auto "> <!-- col-10 col-sm-10 col-md-8 col-lg-8      form-colm pl-xl-5 pr-xl-5 -->
                                    <div class="cont-form m-auto">
                                        <!-- Logo Starts Here -->
                                        <div class="col-12 p-0 mb-4 ccp-logo">
                                            <img src="{{asset('login_assets/img/logo.png')}}" alt="" class="ccp-logo">
                                        </div>
                                        <!-- Logo Ends Here -->

                                        @if(Session::has('render')  && Session::has('api_messages'))
                                            <br/>
                                            <div class="alert alert-danger ccp-alert-danger" role="alert">
                                                   {{Session::get('api_messages')}}
                                            </div>
                                        @else
                                            <!-- Form Starts Here -->
                                          <form action="{{ route('LoginComponent.postLogin',(request()->has('redirect_to')?['redirect_to'=>request()->input('redirect_to')]:[]) )}}" method="POST" id="loginForm">
                                                {{ csrf_field() }}
                                                {{--<input type="hidden" name="portal_type" value="customer">--}}
                                                <!-- Form Fields Starts Here -->
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input _ngcontent-c0="" class="form-control form-control-md ccp-txtbox {{ $errors->has('email') ? 'is-invalid' : '' }}"  type="text" name="email" id="email" value="{{((old('email')!==NULL)?old('email'):((request()->email)?request()->email:''))}}" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input class="form-control form-control-md ccp-txtbox {{ $errors->has('password') ? 'is-invalid' : '' }}"  type="password" name="password" id="password">
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class=" btn btn-md btn-block login-btn text-white">Log In</button>
                                                </div>
                                                <div class="form-group">
                                                    <div class="forgot-pass pb-3 text-center block-timer-class">
                                                        <a href="{{  route('LoginComponent.forgotPassword') }}" data-value="{{  route('LoginComponent.forgotPassword') }}" id="forgetUrl">
                                                            Forgot Password?
                                                        </a>
                                                    </div>
                                                </div>

                                                @if(Session::has('api_messages'))
                                                    <div class="alert alert-danger ccp-alert-danger" role="alert">
                                                           {{Session::get('api_messages')}}
                                                    </div>
                                                @endif

                                                @if(Session::has('successMessage'))
                                                    <div class="alert alert-success ccp-alert-success" role="alert">
                                                           {{Session::get('successMessage')}}
                                                    </div>
                                                @endif

                                                @if(count($errors) === 0 && !Session::has('api_messages') && !Session::has('api_messages') && isset(request()->message) && request()->message !== '')
                                                    <div class="alert alert-success ccp-alert-success" role="alert">
                                                           {{request()->message}}
                                                    </div>
                                                @endif

                                                <div class="form-group row mt-2">
                                                    <div class="col-12 mt-0">
                                                        <span class="customer-link">Register to Connection Cloud Platform</span>
                                                    </div>
                                                    <div class="col-12 mt-2">
                                                        <a class="btn btn-primary-outline btn-md btn-block" href="{{ route('SignupComponent.signup') }}">Create Account</a>
                                                        {{--<a class="btn btn-md btn-block login-btn text-white" href="{{ route('SignupComponent.signup') }}">Create Account</a>--}}
                                                    </div>
                                                </div>
                                                <!-- Form Fields Ends Here -->
                                            </form>
                                            <!-- Form Ends Here -->
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Form Container Ends Here -->
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 login-form-text float-left float-sm-right h-sm-auto  pt-5 pb-5 mt-4 mt-md-0"><!-- pt-3 pb-3 pt-md-5 pb-md-5 -->
                <div class="h-100">
                    <div class="row align-items-center h-100 h-sm-auto">
                        <!-- Login Page Text Container Starts Here -->
                        <div id="containertxt" class="container">
                            <div class="row">
                                <div class="m-auto text-white"> <!-- col-12 col-sm-8 col-md-8 col-lg-8 -->
                                    <div class="cont-form m-auto">
                                        <h1 class="text-center  text-md-left">#wesolveIT</h1>
                                        <p class="login-para text-center  text-md-left">
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took a galley of type and scrambled it to make a type specimen book
                                        </p>
                                        <button class="btn btn-success-custom btn-block">Learn More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Login Page Text Container Ends Here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="ccp-footer">
    <div class="col-12">
        <p class="float-md-right mr-auto ml-auto mr-md-3">©2018 Connection Cloud Platform. All rights reserved.</p>
    </div>
</footer>
<script>
    $( "#email" ).on( "blur", function(){
        emailQueryString();
    });
    function emailQueryString() {
        var urlForForget = $("#forgetUrl").attr('data-value');
        var value = $("#email").val();
        var url = urlForForget+"?email="+value;
        $("#forgetUrl").attr("href", url);
    }
    $(window).on('pageshow', function(){
        if($("input#email").val() === '')
            $("input#email").focus();
        else if($("input#password").val() === '')
            $("input#password").focus();
    });
</script>
</body>
</html>
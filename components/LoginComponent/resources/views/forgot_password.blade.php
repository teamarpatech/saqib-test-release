<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>CCP Forgot Password</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('login_assets/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('login_assets/css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('login_assets/css/mediaqurries.css')}}" rel="stylesheet">

</head>
<body>
<div class="container-fluid main-container h-sm-auto">
    <div class="row justify-content-center h-100 h-sm-auto">
        <div class="col-sm-12 p-0 h-sm-auto">
           <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 hidden-md-down login-form-cont float-right float-sm-left h-sm-auto pt-5 pt-md-3 pb-3 pt-md-5 pb-md-5"> <!-- col-sm-6 col-md-10 col-lg-8 col-xl-8 -->

                <div class="h-100">
                    <div class="row align-items-center h-100 h-sm-auto">
                        <!-- Form Container Starts Here -->
                        <div id="container" class="container">
                            <div class="row">
                                <div class="m-auto "> <!-- col-10 col-sm-10 col-md-8 col-lg-8      form-colm pl-xl-5 pr-xl-5 -->
                                    <div class="cont-form m-auto">
                                        <!-- Logo Starts Here -->
                                        <div class="col-12 p-0 mb-4 ccp-logo">
                                            <img src="{{asset('login_assets/img/logo.png')}}" alt="" class="ccp-logo">
                                        </div>
                                        <!-- Logo Ends Here -->

                                        @if(session('message'))
                                            <p class="alert alert-success">{{session('message')}}</p>
                                        @endif

                                        <!-- Form Starts Here -->
                                        @if(empty($successMessage))
                                            <form action="{{url('auth/forgot-password')}}" method="post">
                                                {{csrf_field()}}
                                                <!-- Form Fields Starts Here -->

                                                <div class="form-group">
                                                    <label>Please enter email associated with your account</label>
                                                    <input _ngcontent-c0="" class="form-control form-control-md @if(count($errors) > 0 || !empty($errorMessage)) border-danger @endif"  type="text" name="email" placeholder="email address" value="@if(!empty($email)){{$email}}@endif">
                                                </div>

                                                @if(count($errors) > 0)
                                                    @foreach($errors->all() as $error)
                                                        <div class="form-group">
                                                            <br>
                                                            <div class="alert alert-danger ccp-alert-danger" role="alert">
                                                                {{$error}}
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif

                                                @if(!empty($errorMessage))
                                                    <br>
                                                    <div class="alert alert-danger ccp-alert-danger" role="alert">
                                                        {{$errorMessage}}
                                                    </div>
                                                @endif

                                                <div class="form-group">
                                                    <button class=" btn btn-md btn-block login-btn text-white">Submit</button>
                                                </div>
                                                <!-- Form Fields Ends Here -->
                                            </form>
                                        @else
                                            <div class="alert alert-success ccp-alert-success" role="alert">
                                                {{$successMessage}}
                                            </div>
                                            <div class="after-forgot-password">
                                                <a href="{{url('auth/login')}}">
                                                    <img src="{{asset('img/backArrow.png')}}" alt="" class="ccp-logo">
                                                    <span>Go Back to Login</span>
                                                </a>
                                            </div>
                                        @endif
                                        <!-- Form Ends Here -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Form Container Ends Here -->
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 login-form-text float-left float-sm-right h-sm-auto pt-5 pb-5 mt-4 mt-md-0"><!-- col-sm-6 col-md-10 col-lg-8 col-xl-8 -->

                <div class="h-100">
                    <div class="row align-items-center h-100 h-sm-auto">
                        <!-- Login Page Text Container Starts Here -->
                        <div id="containertxt" class="container">
                            <div class="row">
                                <div class="m-auto text-white"> <!-- col-12 col-sm-8 col-md-8 col-lg-8 -->
                                    <div class="cont-form m-auto">
                                        <h1 class="text-center  text-md-left">#wesolveIT</h1>
                                        <p class="login-para text-center  text-md-left">
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took a galley of type and scrambled it to make a type specimen book
                                        </p>
                                        <button class="btn btn-success-custom btn-block">Learn More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Login Page Text Container Ends Here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="ccp-footer">
    <div class="col-12">
        <p class="float-md-right mr-auto ml-auto mr-md-3">©2018 Connection Cloud Platform. All rights reserved.</p>
    </div>
</footer>

</body>
</html>
<?php
Route::group(['middleware' => 'guest'], function() {
	Route::get('account/create-account','SignupController@index')->name('signup');
	Route::post('post-signup-step1','SignupController@postSignupStep1')->name('postSignupStep1');
	Route::post('post-send-email','SignupController@postSendEmail')->name('postSendEmail');
	Route::get('verify-email/{token}/{emailToken}','SignupController@verifyEmail')->name('verifyEmail');
	Route::post('post-verify-code','SignupController@postVerifyCode')->name('postVerifyCode');
	Route::post('get-industry-types','SignupController@getIndustryTpes')->name('getIndustryTpes');
	Route::post('get-comapny-sizes','SignupController@getComapnySizes')->name('getComapnySizes');
	Route::post('add-business-information','SignupController@addBusinessInformation')->name('addBusinessInformation');
	Route::post('get-job-titles','SignupController@getJobTitles')->name('getJobTitles');
	Route::post('add-user-information','SignupController@addUserInformation')->name('addUserInformation');
	Route::post('get-subscription-plans','SignupController@getSubscriptionPlans')->name('getSubscriptionPlans');
	Route::post('update-subscription-plan','SignupController@updateSubscriptionPlan')->name('updateSubscriptionPlan');
	Route::post('get-license-agreement','SignupController@getLicenseAgreement')->name('getLicenseAgreement');
	Route::post('update-company-agreement','SignupController@updateCompanyAgreement')->name('updateCompanyAgreement');
	Route::post('post-request-access-email','SignupController@postRequestAccessEmail')->name('postRequestAccessEmail');
    Route::get('verify-invite/{token}','SignupController@verifyInvite')->name('verifyInvite');
    Route::post('signup-invited-user','SignupController@signupInvitedUser')->name('postSignupInvitedUser');
    Route::post('get-states','SignupController@getStates')->name('get-states');
    Route::post('post-create-address','SignupController@postCreateAddress')->name('post-create-address');

    Route::get('get-industry-types','SignupController@getIndustryTpes')->name('getIndustryTpes');
    Route::get('get-job-titles','SignupController@getJobTitles')->name('getJobTitles');
    Route::get('get-comapny-sizes','SignupController@getComapnySizes')->name('getComapnySizes');
    Route::get('get-subscription-plans','SignupController@getSubscriptionPlans')->name('getSubscriptionPlans');
    Route::get('get-license-agreement','SignupController@getLicenseAgreement')->name('getLicenseAgreement');

});
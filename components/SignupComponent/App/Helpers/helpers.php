<?php
/**
 * filtering response by encoding
 */
if (! function_exists('filter_response')) {
	function filter_response($response) {
		return json_decode(json_encode(optional($response)['response']), true);
	}
}
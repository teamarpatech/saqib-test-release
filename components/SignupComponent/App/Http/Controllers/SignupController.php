<?php

namespace Component\SignupComponent\App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SignupController extends Controller
{

    /**
     * load signup view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public
    function index()
    {
        $states = collect(optional(filterResponse(handleResponse(GetAPI('account', 'GET', 'getStates', []))))['data']);
        $states = $states->filter(function ($value, $key) {
            if(!is_string($key))
                return $value;
        });
        return view('SignupComponent::index', compact('states'));
    }

    /**
     * signup step 1 to get token, email token and code
     * @param Request $request
     * @return array
     */
    public
    function postSignupStep1(Request $request)
    {
        $login_link = '<a href="' . url('/') . '/auth/login?email=' . $request->email . '">Login</a>';
        $forgot_password_link = '<a href="' . url('/') . '/auth/forgot-password?email=' . $request->email . '">Forgot Password</a>';
        $data = filter_response(handleResponse(GetAPI('account', 'POST', 'signupStep1', $request->only('email', 'portal_type'))));
        if (isset($data['message']) && is_string($data['message'])) {
            $data['message'] = str_replace('[LOGIN_LINK]', $login_link, $data['message']);
            $data['message'] = str_replace('[FORGOT_PASSWORD_LINK]', $forgot_password_link, $data['message']);
        }
        return $data;
    }

    /**
     * send email with token, email token and code to provided email
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function postSendEmail(Request $request)
    {
        $params['to']['name'] = '';
        $params['to']['email'] = $request->email;
        $params['from']['name'] = config('email_templates.email_address_verification.from_name');
        $params['from']['email'] = config('email_templates.email_address_verification.from_email');
        $params['subject']['title'] = config('email_templates.email_address_verification.subject');
        $params['template']['id'] = config('email_templates.email_address_verification.template_id');
        $params['template_data']['verification_code'] = $request->verificationCode;
        $emailResponse = sendEmailWithTemplate($params);
        if ($emailResponse !== TRUE) {
            return response()->json([
                'data' => [
                    'error_type' => 0
                ],
                'success' => TRUE,
                'message' => 'Email Not Sent Please Try Again.',
                'status_code' => 401
            ]);
        }
        return response()->json([
            'data' => [
                'error_type' => 1
            ],
            'success' => TRUE,
            'message' => 'Verification email sent successfully.',
            'status_code' => 200
        ]);
    }

    /**
     * email verify from email link
     * @param         $token
     * @param         $emailToken
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public
    function verifyEmail($token, $emailToken, Request $request)
    {
        $data = filter_response(handleResponse(GetAPI('account', 'GET', 'validateEmail', ['token' => $token, 'emailToken' => $emailToken, 'portal_type' => $request->portal_type])));
        $email = base64_decode($emailToken);
        $data['data']->email = $email;
        return view('SignupComponent::index', $data);
    }

    /**
     * verify 6 digit code
     * @param Request $request
     * @return array
     */
    public
    function postVerifyCode(Request $request)
    {
        return filter_response(handleResponse(GetAPI('account', 'POST', 'validateEmail', $request->only(['email_token', 'verification_code', 'portal_type']))));
    }

    /**
     * get all industry types for new company
     * @return array
     */
    public
    function getIndustryTpes(Request $request)
    {
        //echo '<pre>'; print_r(filter_response(handleResponse(GetAPI('account', 'GET', 'getIndustryTpes', $request->only('portal_type'))))); echo '</pre>'; exit;
        return filter_response(handleResponse(GetAPI('account', 'GET', 'getIndustryTpes', $request->only('portal_type'))));
    }

    /**
     * get sizes for a new company (employees range)
     * @return array
     */
    public
    function getComapnySizes(Request $request)
    {
        //echo '<pre>'; print_r(filter_response(handleResponse(GetAPI('account', 'GET', 'getComapnySizes', $request->only('portal_type'))))); echo '</pre>'; exit;
        return filter_response(handleResponse(GetAPI('account', 'GET', 'getComapnySizes', $request->only('portal_type'))));
    }

    /**
     * add new business (new company) or request access to a business (company)
     * @param Request $request
     * @return array
     */
    public
    function addBusinessInformation(Request $request)
    {
        return filter_response(handleResponse(GetAPI('account', 'POST', 'addBusinessInformation', $request->only(['company_id', 'email', 'company_name', 'company_type_id', 'company_size_id', 'portal_type']))));
    }

    /**
     * get all job titles
     * @return array
     */
    public
    function getJobTitles(Request $request)
    {
        //echo '<pre>'; print_r(filter_response(handleResponse(GetAPI('account', 'GET', 'getJobTitles', $request->only('portal_type'))))); echo '</pre>'; exit;
        return filter_response(handleResponse(GetAPI('account', 'GET', 'getJobTitles', $request->only('portal_type'))));
    }

    /**
     * add user information
     * @param Request $request
     * @return array
     */
    public
    function addUserInformation(Request $request)
    {
        return filter_response(handleResponse(GetAPI('account', 'POST', 'addUserInformation', $request->only(['email', 'first_name', 'last_name', 'phone', 'job_title_id', 'password', 'role_name', 'portal_type']))));
    }

    /**
     * get all subscription plans
     * @return array
     */
    public
    function getSubscriptionPlans(Request $request)
    {
        //echo '<pre>'; print_r(filter_response(handleResponse(GetAPI('account', 'GET', 'getSubscriptionPlans', $request->only('portal_type'))))); echo '</pre>'; exit;
        return filter_response(handleResponse(GetAPI('account', 'GET', 'getSubscriptionPlans', $request->only('portal_type'))));
    }

    /**
     * update a subscription plan
     * @param Request $request
     * @return array
     */
    public
    function updateSubscriptionPlan(Request $request)
    {
        return filter_response(handleResponse(GetAPI('account', 'POST', 'updateSubscriptionPlan', $request->only(['email', 'company_id', 'subscription_plan_id', 'portal_type']))));
    }

    /**
     * get license agreement
     * @return array
     */
    public
    function getLicenseAgreement(Request $request)
    {
        //echo '<pre>'; print_r(filter_response(handleResponse(GetAPI('account', 'GET', 'getLicenseAgreement', $request->only('portal_type'))))); echo '</pre>'; exit;
        return filter_response(handleResponse(GetAPI('account', 'GET', 'getLicenseAgreement', $request->only('portal_type'))));
    }

    /**
     * update company agreement
     * @param Request $request
     * @return array
     */
    public
    function updateCompanyAgreement(Request $request)
    {
        $data = filter_response(handleResponse(GetAPI('account', 'POST', 'updateCompanyAgreement', $request->only(['email', 'company_id', 'license_id', 'portal_type']))));
        if(intval($data['data']['error_type']) === 1) // error type 1 is equal to success in apis
            $data['email_sent'] = $this->sendCompleteUserRegistrationWithNewCompanyEmail($request);
        return $data;
    }

    /**
     * email to user and manager on requesting access (selecting existing company)
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function postRequestAccessEmail(Request $request)
    {
        /**
         * Business Email
         */
        $params['to']['name'] = '';
        $params['to']['email'] = $request->business_email;
        $params['from']['name'] = config('email_templates.account_access_pending.from_name');
        $params['from']['email'] = config('email_templates.account_access_pending.from_email');
        $params['subject']['title'] = config('email_templates.account_access_pending.subject');
        $params['template']['id'] = config('email_templates.account_access_pending.template_id');
        $params['template_data']['company_name'] = $request->company_name;
        $emailResponse = sendEmailWithTemplate($params);
        if ($emailResponse !== TRUE) {
            return response()->json([
                'data' => [
                    'error_type' => 0
                ],
                'success' => TRUE,
                'message' => 'Account pending email not sent please try again.',
                'status_code' => 401
            ]);
        }
        /**
         * Manager Email
         */
        $params['to']['name'] = '';
        $params['to']['email'] = $request->account_manager_email;
        $params['from']['name'] = config('email_templates.account_access_requested.from_name');
        $params['from']['email'] = config('email_templates.account_access_requested.from_email');
        $params['subject']['title'] = config('email_templates.account_access_requested.subject');
        $params['template']['id'] = config('email_templates.account_access_requested.template_id');
        $params['template_data']['company_name'] = $request->company_name;
        $params['template_data']['business_email'] = $request->business_email;
        $emailResponse = sendEmailWithTemplate($params);
        if ($emailResponse !== TRUE) {
            return response()->json([
                'data' => [
                    'error_type' => 0
                ],
                'success' => TRUE,
                'message' => 'Account requested email not sent please try again.',
                'status_code' => 401
            ]);
        }
        return response()->json([
            'data' => [
                'error_type' => 1
            ],
            'success' => TRUE,
            'message' => 'Account requested and Account pending email has been sent successfully.',
            'status_code' => 200
        ]);
    }

    /**
     * verify invite from email link
     * @param         $token
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public
    function verifyInvite($token, Request $request)
    {
        $data = filter_response(handleResponse(GetAPI('account', 'GET', 'verifyInvite', ['token' => $token, 'portal_type' => $request->portal_type])));
        $data['error_user_invite'] = ((count($data['data']) > 0)?0:1);
        $data['next_step'] = 4;
        $data['is_user_invite'] = 1;
        return view('SignupComponent::index', $data);
    }

    /**
     * signup invited user
     * @param Request $request
     * @return array
     */
    public
    function signupInvitedUser(Request $request)
    {
        return filter_response(handleResponse(GetAPI('account', 'POST', 'signupInvitedUser', $request->only(['email', 'first_name', 'last_name', 'phone', 'job_title_id', 'password', 'role_name', 'portal_type']))));
    }

    /**
     * email to user who completed the signup with new company
     * @param Request $request
     * @return bool
     */
    public
    function sendCompleteUserRegistrationWithNewCompanyEmail(Request $request)
    {
        /**
         * Complete Registration Email
         */
        $params['to']['name'] = $request->first_name. ' ' . $request->last_name;
        $params['to']['email'] = $request->email;
        $params['from']['name'] = config('email_templates.successful_user_registration_with_new_company.from_name');
        $params['from']['email'] = config('email_templates.successful_user_registration_with_new_company.from_email');
        $params['subject']['title'] = config('email_templates.successful_user_registration_with_new_company.subject');
        $params['template']['id'] = config('email_templates.successful_user_registration_with_new_company.template_id');
        $params['template_data']['user_name'] = $request->first_name. ' ' . $request->last_name;
        $params['template_data']['login_link'] = url("/auth/login?email=".$request->email);
        try{
            $emailResponse  = sendEmailWithTemplate($params);
            if ($emailResponse === true) {
                return true;
            }
            return false;
        }catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

    /**
     * get state list
     * @return mixed
     */
    public
    function getStates()
    {
        return filterResponse(handleResponse(GetAPI('account', 'GET', 'getStates', [])));
    }

    /**
     * create new address
     * @return mixed
     */
    public
    function postCreateAddress(Request $request)
    {
        return filterResponse(handleResponse(GetAPI('account', 'POST', 'createAddress', $request->only(['organization_name', 'address_line_1', 'address_line_2', 'city', 'state', 'zip_code', 'phone', 'ext', 'type', 'account_id', 'email']))));
    }

}
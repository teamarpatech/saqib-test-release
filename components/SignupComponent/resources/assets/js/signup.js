window._ = require('lodash');
require('bootstrap');
window.Vue = require('vue');
import VueResource      from 'vue-resource';

Vue.use(VueResource);
Vue.http.options.root = root_url;  // api server domain
import Vue              from 'vue';
import VueTheMask       from 'vue-the-mask';
Vue.use(VueTheMask);
var VueScrollTo = require('vue-scrollto');
Vue.use(VueScrollTo, {
	container: "body",
	duration: 500,
	easing: "ease",
	offset: 0,
	force: true,
	cancelable: true,
	onStart: false,
	onDone: false,
	onCancel: false,
	x: false,
	y: true
});


let token = document.head.querySelector('meta[name="csrf-token"]').content;
if (token)
	Vue.http.headers.common['X-CSRF-TOKEN'] = token;
else
	console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');

import VeeValidate      from 'vee-validate';
const dict = {
	custom: {
		email: {
			required: 'Please provide your business email address',
		},
		newcompanyname: {
			required: 'Please provide the company name in order to proceed',
		},
		industrytype: {
			required: 'Please select a valid Industry Type',
		},
		newcompanysizes: {
			required: 'Please select a valid Number of employees',
		},
		jobtitle: {
			required: 'Please select a valid Job Title',
		},
	},
};
VeeValidate.Validator.localize('en', dict);
//custom validation rules
const customPhone = {
	getMessage(field, args) {
		return `The ${field} field must follow this format (xxx.xxx.xxxx)`;
	},
	validate(value, args) {
		// Custom regex for a phone number
		const MOBILEREG = /^[0-9]{3}\.[0-9]{3}\.[0-9]{4}$/;
		// Check for either of these to return true
		return MOBILEREG.test(value);
	},
};
const customPassword = {
	getMessage(field, args) {
		return `The ${field} must contain an upper-case letter, a lower-case letter & a number.`;
	},
	validate(value, args) {
		// Custom regex for a password
		const PassEREG = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/;
		// Check for either of these to return true
		return PassEREG.test(value);
	},
};
const alphaNumericWithSpace = {
	getMessage(field, args) {
		return `The ${field} can only contain alphabates and numbers.`;
	},
	validate(value, args) {
		// Custom regex for a password
		const PassEREG = /^[-\w ]+$/;
		// Check for either of these to return true
		return PassEREG.test(value);
	},
};
const customZip = {

    getMessage(field, args) {
        return `The ${field} field should be (xxxxx) or (xxxxx xxxx)`;
    },

    validate(value, args) {

        let result = false;

        if (value.length == 5 || value.length == 6 || value.length == 10) {
            result = true;
        }

        return result;
    },
};
const alpha_num_spaces = {

    getMessage(field, args) {
        return `The ${field} field only contain alphabates numbers and spaces.`;
    },

    validate(value, args) {
        // Custom regex for a phone number
        const MOBILEREG = /^[a-zA-Z0-9\s]+$/;
        // Check for either of these to return true
        return MOBILEREG.test(value);
    },
};
// Extend VeeValidate with our new custom rule
VeeValidate.Validator.extend('cPassword', customPassword);
VeeValidate.Validator.extend('cPhone', customPhone);
VeeValidate.Validator.extend('alphaNumericWithSpace', alphaNumericWithSpace);
VeeValidate.Validator.extend('cZip', customZip);
VeeValidate.Validator.extend('alpha_num_spaces', alpha_num_spaces);

const config = {
	errorBagName: 'errorBag', // change if property conflicts.
	//dictionary:  veeCustomMessage,
	events: 'input',
};
Vue.use(VeeValidate, config);
window.EventBus = new Vue({});
// Vue.http.options.emulateJSON = true;
Vue.component('alert', require('../components/alert.vue'));
import emailInfo        from '../components/emailInfo';
import businessInfo     from '../components/businessInfo';
import userInfo         from '../components/userInfo';
import subscription     from '../components/subscription';
import addressOfUse     from '../components/addressOfUse';
import licenseAgreement from '../components/licenseAgreement';
import isInViewPort   	from './checkIsInViewport';

// inject a helper with global mixin
Vue.mixin({
	methods: {
		// show all type of messages
		handleMessage: function (showMessage, messageClass, messageText) {
			this.messageText = '';
			this.messageClass = '';
			this.showMessage = false;
			if (typeof messageText === 'object') {
				let arr = _.values(messageText);
				messageText = _.toString(arr[0]);
			}
			this.messageText = messageText;
			this.messageClass = messageClass;
			this.showMessage = showMessage;
			setTimeout(()=>{
				let element = document.querySelector('.all-notifications');
				if(element !== null && element !== undefined) {
					if (!isInViewPort(element)) {
						let options = {
							offset: -100
						};
						// VueScrollTo.scrollTo('.all-notifications', this.set_time_out_wait, options);
					}
				}
			// 	// this.$scrollTo('.all-notifications', this.set_time_out_wait);
			// 	// var cancelScroll = VueScrollTo.scrollTo('.all-notifications', duration, options)
			// 	// // or alternatively inside your components you can use
			// 	// cancelScroll = this.$scrollTo('.all-notifications', duration, options)
			// 	// // to cancel scrolling you can call the returned function
			// 	// cancelScroll()
			}, 500);
		},
	}
});
Vue.directive('focus', {
    inserted : function (el) {
        el.focus()
    }
});
var app = new Vue({
	el: '#ccp_app',
	data: {
		emailbox: false,
		step1: false,
		step12: false,
		step2: false,
		step3: false,
		step4: false,
		step5: false,
		step6: false,
		currentStep: '',
		user_email: email,
		companies: [],
		UserCompanyName: '',
		UserCompanyIndustryType: '',
		UserFirstName: '',
		UserLastName: '',
		UserPhone: '',
		UserJobType: '',
		UserPackage: '',
		subscriptionPlanName: '',
		stepGrid1: false,
		stepGrid2: false,
		stepGrid3: false,
		stepGrid4: false,
		stepGrid5: false,
		stepGrid6: false,
		licenseId: false,
		businessStep: true,
		UserInfoStep: true,
		SubscriptionStep: true,
		AddressOfUseStep: true,
		LicenseAgreementStep: true,
		company_id: '',
		is_new_company_selected: true,
		create_account_existing_company: false,
        create_account_invite_user: false,
		set_time_out_wait: 500,
		allow_more_accounts: true,
        root_url: '',
        error_user_invite: 0,
        error_user_invite_message: '',
        status_code: '',
        is_user_invite: 0,
        loaded: false,
        RoleName: '',
        states: states,
        AddressOfUseOrganizationName: '',
		AddressOfUseAddressLine1: '',
		AddressOfUsePhone: ''
    },
	components: {emailInfo, businessInfo, userInfo, addressOfUse, subscription, licenseAgreement},
	mounted() {
		this.loaded = true;
		this.user_email = email;
		this.companies = companies;
		this.error_user_invite = error_user_invite;
		this.error_user_invite_message = error_user_invite_message;
		this.status_code = status_code;
		this.is_user_invite = is_user_invite;
		this.root_url = root_url;
		this.disableAllSteps();
		if (step === 1) {
			this.step1 = true;
		}
		else if (step === 2) {
			this.step1 = true;
			this.step12 = true;
		}
		else if (step === 3) {
			this.step2 = true;
		}
		else if (step === 4) {
			if (is_user_invite === 1) {
                this.create_account_invite_user = true;
                this.UserCompanyName = company_name;
                this.UserCompanyIndustryType = company_industry_type;
                this.step1Complete();
                this.step2Complete();
			}
			this.step3 = true;
		}
		else if (step === 5) {
			this.step4 = true;
		}
		else if (step === 6) {
			this.step5 = true;
		}
		else if (step === 7) {
			this.step6 = true;
		}
		this.currentStepGet();
		/*Step 1 complete*/
		window.EventBus.$on('onClickStep1', (data) => {
			this.user_email = data.user_email;
			this.companies = data.companies;
			this.allow_more_accounts = data.allow_more_accounts;
			this.step1Complete();
		});
		/*Step 2 complete*/
		window.EventBus.$on('onClickStep2', (data) => {
			this.UserCompanyName = data.companyName;
			this.company_id = data.company_id;
			this.is_new_company_selected = data.is_new_company_selected;
			if (data.hasOwnProperty('selectedIndustryTypeName') && data.selectedIndustryTypeName != undefined) {
				this.UserCompanyIndustryType = data.selectedIndustryTypeName;
			}
			this.step2Complete();
		});
		window.EventBus.$on('updateCompanyType', (data) => {
			this.is_new_company_selected = data.is_new_company_selected;
		});
		/*Step  3 Complete*/
		window.EventBus.$on('onClickStep3', (data) => {
			this.UserFirstName = data.FirstName;
			this.UserLastName = data.LastName;
			this.UserPhone = data.Phone;
			this.UserJobType = data.JobTitle;
			this.step3Complete();
		});
		/*Step 4 Complete*/
		window.EventBus.$on('OnClickStep4', (data) => {
            this.AddressOfUseOrganizationName = data.organization_name;
            this.AddressOfUseAddressLine1 = data.address_line_1;
            this.AddressOfUsePhone = data.phone;
			this.step4Complete();
		});
		/*Step 5 Complete*/
		window.EventBus.$on('OnClickStep5', (data) => {
			this.UserPackage = data.subscriptionPlanId;
			this.subscriptionPlanName = data.subscriptionPlanName;
			this.step5Complete();
		});
		/*License Agreement Change Event*/
		window.EventBus.$on('LicenseAgreementChange', (data) => {
			this.licenseId = data.srcElement.checked;
		});
		/*Disable Enable Create Account Button*/
		window.EventBus.$on('gridCreateAccountButton', (data) => {
			this.create_account_existing_company = false;
		});
		/*Disable Enable Create Account Button At Invite User Case*/
		window.EventBus.$on('gridInviteCreateAccountButton', (data) => {
			this.error_user_invite = data.error_user_invite;
		});
        window.EventBus.$on('UserRoleUpdate', (data) => {
            this.RoleName = data.role_name;
        });


	},
	methods: {
        disableAllSteps(){
			this.step1 	= false;
            this.step12 = false;
            this.step2 	= false;
            this.step3 	= false;
            this.step4 	= false;
            this.step5 	= false;
		},
		step1Complete() {
			this.step1 = false;
			this.step2 = true;
			this.emailbox = true;
			this.stepGrid1 = true;
			this.businessStep = false;
			this.currentStepGet();
		},
		step2Complete() {
			this.step2 = false;
			this.step3 = true;
			this.stepGrid2 = true;
			this.UserInfoStep = false;
			if (this.is_new_company_selected == false)
				this.create_account_existing_company = true;
			else
				this.create_account_existing_company = false;
			this.currentStepGet();
		},
		step3Complete() {
			this.AddressOfUseStep = false;
			this.step3 = false;
			this.step4 = true;
			this.stepGrid3 = true;
			this.currentStepGet();
		},
		step4Complete() {
            this.SubscriptionStep = false;
			this.step4 = false;
			this.step5 = true;
			this.stepGrid4 = true;
			this.currentStepGet();
		},
		step5Complete() {
            this.LicenseAgreementStep = false;
			this.step5 = false;
			this.step6 = true;
			this.stepGrid5 = true;
			this.currentStepGet();
		},
		currentStepGet() {
			if (this.step1) {
				this.currentStep = 'step1';
			}
			else if (this.step2) {
				this.currentStep = 'step2';
			}
			else if (this.step3) {
				this.currentStep = 'step3';
			}
			else if (this.step4) {
				this.currentStep = 'step4';
			}
			else if (this.step5) {
				this.currentStep = 'step5';
			}
			else if (this.step6) {
				this.currentStep = 'step6';
			}
		},
        ClickLicenseCreateAccount: function () {
			window.EventBus.$emit('LicenseCreateAccountGridClick', {});
		},
		ClickUserCreateAccount: function () {
			window.EventBus.$emit('UserCreateAccountGridClick', {});
		},
        ClickInviteUserCreateAccount: function () {
			window.EventBus.$emit('InviteUserCreateAccountGridClick', {});
		},
	},
});
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Create Account - CCP</title>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Encode+Sans:300,400" rel="stylesheet">
	<!-- Bootstrap core CSS -->
	<link href="{{ asset('components/signup/css/bootstrap.min.css') }}" rel="stylesheet">
	<!--  Font Awesome Library --->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link  href="{{ asset('components/signup/css/signup.css') }}"  rel="stylesheet">
	<link href="{{ asset('components/signup/css/mediaqueries.css') }}"   rel="stylesheet">
	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<!-- Signup page starts here -->
<!-- Header Starts Here -->
<header class="ccp_header">
	<div class="container" >
		<div class="ccp_logo">
			<img src="{{ asset('images/logo.png') }}" alt="">
		</div>
	</div>
</header>
<!-- Header Ends Here -->
<!-- Signup Form Area Starts Here -->
<!-- Main Container Starts Here -->
<div id="ccp_app" class="container ccp-main-cont">
	<div v-cloak class="main-heading">
		Unlock the Connection Advantage
	</div>
	<div v-cloak class="row">
		<!-- Form Grid Starts Here -->
		<div  class=" col-sm-12 col-md-8">
			<!-- Email Info Starts Here -->
			<!-- Email Info Box After Submit Starts Here -->
			<div v-if="emailbox" class=" detail-box email-details increment-num">
				 <span class="step-title">
					 Email Information
				 </span>
				<div class="ccp-info-desc">
                    <span class="ccp-form-action">
                        {{--<a href="#">Restart</a>--}}
                    </span>
					<span class="ccp-section-field" v-text="user_email"></span>
				</div>
			</div>
			<!-- Email Info Box After Submit Ends Here -->
			<!-- Business Info Info Box After Submit Starts Here -->
			<div v-if="stepGrid2" class="detail-box email-details increment-num">
                 <span class="step-title">
                     Business Information
                 </span>
				<div class="ccp-info-desc">
                    <span class="ccp-form-action">
                        {{--<a href="#">Edit</a>--}}
                    </span>
					<span class="ccp-section-field">  <span v-text="UserCompanyName"></span> <br> <span v-text="UserCompanyIndustryType"></span>  </span>
				</div>
			</div>
			<!-- Email Info Box After Submit Ends Here -->
			<!-- User  Info Box After Submit Starts Here -->
			<div v-if="stepGrid3" class=" detail-box email-details increment-num">
                 <span class="step-title">
                     User Information
                 </span>
				<div class="ccp-info-desc">
                    <span class="ccp-form-action">
                        {{--<a href="#">Edit</a>--}}
                    </span>
					<span class="ccp-section-field">
						<span v-text="UserFirstName"></span>&nbsp;<span v-text="UserLastName"></span>
						<br>
						<span v-text="UserPhone"></span>
						<br>
						<span v-text="UserJobType"></span>
					</span>
				</div>
			</div>
			<!-- User Info Box After Submit Ends Here -->
			<!-- Subscription Box After Submit Starts Here -->
			<div v-if="stepGrid4" class=" detail-box email-details increment-num">
                 <span class="step-title">
                     Address of Use
                 </span>
				<div class="ccp-info-desc">
                    <span class="ccp-form-action">
                        {{--<a href="#">Edit</a>--}}
                    </span>
					<span class="ccp-section-field">
						<span v-text="AddressOfUseOrganizationName"></span>
						<br>
						<span v-text="AddressOfUseAddressLine1"></span>
						<br>
						<span v-text="AddressOfUsePhone"></span>
					</span>
				</div>
			</div>
			<!-- Subscription Box After Submit Ends Here -->
			<!-- Subscription Box After Submit Starts Here -->
			<div v-if="stepGrid5" class=" detail-box email-details increment-num">
                 <span class="step-title">
                     Subscription
                 </span>
				<div class="ccp-info-desc">
                    <span class="ccp-form-action">
                        {{--<a href="#">Edit</a>--}}
                    </span>
					<span class="ccp-section-field" v-text="subscriptionPlanName"></span>
				</div>
			</div>
			<!-- Subscription Box After Submit Ends Here -->
			<email-info v-if="currentStep=='step1'" :root_url="root_url" :step12="step12" :set_time_out_wait="set_time_out_wait"></email-info>
			<!-- Email Info Ends Here -->

			<!-- Business Info Starts Here -->
			<div id="step2" v-if="businessStep"  class="ccp-form-step ccp-business-info increment-num">
				<div class="step-disabled">
                    <span class="step-title">
                      Business Information
                    </span>
				</div>
			</div>
			<!-- Buinsess Info Form Starts Here -->
			<business-info v-if="currentStep=='step2'" :root_url="root_url" :user_email="user_email" :companies="companies" :allow_more_accounts="allow_more_accounts" :set_time_out_wait="set_time_out_wait"></business-info>
			<!-- Buinsess Info Form End Here -->
			<!-- Business Info Ends Here -->

            <!-- User Info Starts Here -->
			<div id="step3" v-if="UserInfoStep" class="ccp-form-step ccp-user-info increment-num">
				<div class="step-disabled">
				   <span class="step-title">
					  User Information
					</span>
				</div>
			</div>

			<!-- User Info Form Starts Here -->
			<user-info v-if="currentStep=='step3'" :role_name="RoleName" :status_code="status_code" :error_user_invite_message="error_user_invite_message" :error_user_invite="error_user_invite" :root_url="root_url" :user_email="user_email" :is_user_invite="is_user_invite" :is_new_company_selected="is_new_company_selected" :set_time_out_wait="set_time_out_wait"></user-info>
			<!-- User Info Form End Here -->
			<!-- User Info Ends Here -->

			<div v-if="is_new_company_selected && parseInt(is_user_invite) === 0">

				<!-- Address of Use Starts Here -->
				<div id="step4" v-if="AddressOfUseStep" class="ccp-form-step ccp-user-info increment-num">
					<div class="step-disabled">
						<span class="step-title">
						Address of Use
						</span>
					</div>
				</div>
				<!-- Address of Use Form Starts Here -->
				<address-of-use v-if="currentStep=='step4'" :states="states" :root_url="root_url" :user_email="user_email" :company_id="company_id" :set_time_out_wait="set_time_out_wait"></address-of-use>
				<!-- Address of Use Form End Here -->
				<!-- Address of Use Ends Here -->

				<!-- Subscription Starts Here -->
				<div id="step5" v-if="SubscriptionStep" class="ccp-form-step ccp-user-info increment-num">
					<div class="step-disabled">
                       <span class="step-title">
                          Subscription
                        </span>
					</div>
				</div>
				<!-- Subscription Form Starts Here -->
				<subscription v-if="currentStep=='step5'" :root_url="root_url" :user_email="user_email" :company_id="company_id" :set_time_out_wait="set_time_out_wait"></subscription>
				<!-- Subscription Info Form End Here -->
				<!-- Subscription Ends Here -->
				<!-- Cloud Service Agreement Starts Here -->
				<div id="step6" v-if="LicenseAgreementStep"  class="ccp-form-step ccp-user-info increment-num">
					<div class="step-disabled">
					   <span class="step-title">
						  Cloud Service Agreement
						</span>
					</div>
				</div>
				<!-- Cloud Service Agreement Form Starts Here -->
				<license-agreement v-if="currentStep=='step6'" :first_name="UserFirstName" :last_name="UserLastName" :root_url="root_url" :user_email="user_email" :company_id="company_id" :set_time_out_wait="set_time_out_wait"></license-agreement>
				<!-- Cloud Service Agreement Form End Here -->
				<!-- Cloud Service Agreement Ends Here -->
			</div>
		</div>
		<!-- Form Grid Ends Here -->
		<!-- Steps Notifications Starts Here -->
		<div class="cont-steps col-md-4 pull-right">
			<!-- Stepper -->
			<div class="ccp_form_steps">
				<ul class="StepProgress">
					<li class="StepProgress-item" :class="[ step1 ? 'active' : '', stepGrid1  ? 'is-done' : 'current'  ]">
						Email Information
					</li> <!-- is-done(class) -->
					<li class="StepProgress-item" :class="[ step2 ? 'active' : '', stepGrid2  ? 'is-done' : 'current'  ]">Business Information</li>
					<li class="StepProgress-item" :class="[ step3 ? 'active' : '', stepGrid3  ? 'is-done' : 'current'  ]">User Information</li>
					<li v-if="is_new_company_selected && parseInt(is_user_invite) === 0" class="StepProgress-item" :class="[ step4 ? 'active' : '', stepGrid4  ? 'is-done' : 'current'  ]">Address of Use</li>
					<li v-if="is_new_company_selected && parseInt(is_user_invite) === 0" class="StepProgress-item" :class="[ step5 ? 'active' : '', stepGrid5  ? 'is-done' : 'current'  ]">Subscription</li>
					<li v-if="is_new_company_selected && parseInt(is_user_invite) === 0" class="StepProgress-item" :class="[ step6 ? 'active' : '', licenseId  ? 'is-done' : 'current'  ]">Cloud Service Agreement</li>
				</ul>
				<div class="cont-accountbtn">
					<button v-if="is_new_company_selected && is_user_invite === 0" @click="ClickLicenseCreateAccount" class="btn btn-default btn-create-account" :class="{'enable-step-btn': licenseId }" :disabled="!licenseId">Create Account</button>
					<button v-if="!is_new_company_selected && loaded && is_user_invite === 0" @click="ClickUserCreateAccount" class="btn btn-default btn-create-account" :class="{'enable-step-btn': create_account_existing_company }" :disabled="!create_account_existing_company">Create Account</button>
					<button v-if="is_user_invite === 1 && loaded" @click="ClickInviteUserCreateAccount" class="btn btn-default btn-create-account" :class="{'enable-step-btn': (error_user_invite === 0) }" :disabled="!create_account_invite_user || error_user_invite === 1">Create Account</button>
				</div>
			</div>
		</div>
		<!-- Steps Notifications Ends Here -->
	</div>
</div>
<!-- Main Container Ends Here -->
<!-- Signup Form Area Ends Here -->
<!-- Signup page ends here -->
<!-- Footer Starts Here -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-6 footer-sec">
				<!-- Footer Logo Starts Here -->
				<div class="footer-logo">
					<img src="{{ asset('images/footer-logo.png') }}" alt="">
				</div>
				<!-- Footer Logo Ends Here -->
				<!-- Social Media Icons Starts Here -->
				<div class="social-media-icons">
                    <span>
                        <a href="http://www.connection.com/social-LinkedIn-ent">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </span>
                    <span>
                        <a href="http://www.connection.com/social-Twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </span>
                    <span>
                        <a href="http://www.connection.com/social-Facebook">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </span>
					<span>
                        <a href="http://www.connection.com/social-Spiceworks">
                            <i class="fa spiceworks fa-2x"></i>
                        </a>
                    </span>
                    <span>
                        <a href="http://www.connection.com/social-Instagram">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </span>
                    <span>
                        <a href="http://www.connection.com/social-YouTube">
                            <i class="fa fa-youtube-play"></i>
                        </a>
                    </span>
				</div>
				<!-- Social Media Icons Ends Here -->
				<!-- Copyrights Starts Here -->
				<div class="copyrights">
					© 2018 Connection Cloud Platform. All rights reserved.
				</div>
				<!-- Copyrights Ends Here -->
			</div>
			<!-- Footer Menu Starts Here -->
			<div class="col-sm-12 col-md-6 footer-menu">
				<ul class="pull-right">
					<li><a href="#">Product & Services</a></li>
					<li><a href="#">Partner</a></li>
					<li><a href="#">Leadership</a></li>
					<li><a href="#">Support</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</div>
			<!-- Footer Menu Ends Here -->
		</div>
	</div>
</footer>
<!-- Footer Ends Here -->
</body>
<script>
    const step                      = parseInt('{{isset($data->next_step)?$data->next_step:(isset($next_step)?$next_step:'1')}}');
    const email                     = '{{isset($data->email)?$data->email:(isset($data['email'])?$data['email']:'')}}';
    const is_user_invite            = parseInt('{{isset($is_user_invite)?$is_user_invite:0}}');
    const error_user_invite         = parseInt('{{isset($error_user_invite)?$error_user_invite:'0'}}');
    const error_user_invite_message = '{{isset($message)?$message:'0'}}';
    const status_code               = parseInt('{{isset($status_code)?$status_code:'0'}}');
    const company_name              = '{{isset($data['company']['name'])?$data['company']['name']:''}}';
    const company_industry_type     = '{{isset($data['company']['industry_type']['title'])?$data['company']['industry_type']['title']:''}}';
    const companies                 = JSON.parse('{!! (isset($data->companies)?json_encode($data->companies):json_encode([])) !!}');
    const states                    = JSON.parse('{!! (isset($states)?json_encode($states):json_encode([])) !!}');
    const root_url                  = '{{url('/')}}';
</script>
<script src="{{ asset('components/signup/js/signup.js') }}"></script>
</html>

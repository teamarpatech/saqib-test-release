<?php

Route::get('/communication', 'CommunicationController@index');

Route::get('notifications', 'NotificationsController@index')->name("notifications");



Route::post('get-all-notification', 'NotificationsController@getAllNotifications')->name("get-all-notification");
Route::post('get-flagged-notification', 'NotificationsController@getFlaggedNotifications')->name("get-flagged-notification");

Route::post('update-notification', 'NotificationsController@updateNotification')->name("update-notification");
Route::post('marked-as-read-notification', 'NotificationsController@markedAsReadNotification')->name("marked-as-read-notification");

Route::get('get-unread-notification-count', 'NotificationsController@getUnreadNotificationCount')->name("get-unread-notification-count");
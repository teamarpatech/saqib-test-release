@extends('layouts.new_master_shop')

@push('styles')
    <link href="{{ mix('components/communication/css/notifications.css') }}" rel="stylesheet">
@endpush

@section('page_title', 'Notifications')
@section('content_shop')

    <div id="notificationsTarget">
        <notifications></notifications>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript" src="{{ mix('components/communication/js/notifications.js') }}"></script>
@endpush
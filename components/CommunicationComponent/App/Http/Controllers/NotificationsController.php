<?php

namespace Component\CommunicationComponent\App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;

class NotificationsController extends Controller
{
    // listing page
    public function index()
    {
        return view('CommunicationComponent::notifications', compact('notifications'));
    }

    // call api to fetch notification list.
    public function getNotificationList($param = '') {

        $collection = collect([
            'user_id' => Session::get('users-resource.id'),
            'account_id' => Session::get('users-resource.company_id'),
            'count' => 100
        ]);

        if($param != '')
        {
            $collection = $collection->merge($param);
        }

        return filter_response(handleResponse(GetAPI('communication', 'GET', 'getNotificationList', $collection->toArray())));
    }

    // fetch all kind of notifications.
    function getAllNotifications() {
        return $this->getNotificationList();
    }

    // fetch only flagged notifications.
    function getFlaggedNotifications() {
        $array = ['flagged' => 1];
        return $this->getNotificationList($array);
    }

    // changed notification status (flagged & read)
    function updateNotification(Request $request) {
        $param['json'][] = $request->only("id", "flagged", "read");
        return filter_response(handleResponse(GetAPI('communication', 'PATCH', 'updateNotification', $param)));
    }

    // marked unread notification as read.
    function markedAsReadNotification(Request $request) {

        $request->validate(['postData' => 'required']);
        $collection = collect($request->only("postData"));
        $param['json'] = $collection['postData'];

        return filter_response(handleResponse(GetAPI('communication', 'PATCH', 'updateNotification', $param)));
    }

    // get unread notifications count.
    function getUnreadNotificationCount() {

        $data = $this->getNotificationList();
        return collect($data['data'])->where('read', '=', 0)->count();
    }
}
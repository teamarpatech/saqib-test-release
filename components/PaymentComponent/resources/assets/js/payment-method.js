import '../../../vue_common/vue-config';
import '../../../vue_common/vue-validation';
import '../../../vue_common/vue-pagination';
import '../../../vue_common/vue-filter';
import '../../../vue_common/vue-mixin';

Vue.component('payment-method-listing', require('../components/paymentMethodListing.vue'));
Vue.component('payment-method-credit-card', require('../components/paymentMethodCreditCard.vue'));


var UsersApp = new Vue({
    el:'#PaymentApp',
    data() {
        return {
            root_url: root_url,
            search_keyword: '',
            roles: [],
            tabValue: 'user',
            total_user_count: '',
            total_invite_count:'',
            total_request_count:'',
            pm_listing:true,
            pm_cc:false,
            drop_down_condition:'default'
        }
    },
    mounted(){
    let self=this;
        window.EventBus.$on('showPaymentForm', (data) => {
            self.drop_down_condition = data.dropDownCondition;
            self.pm_cc=true;
            self.pm_listing=false;
        });
        window.EventBus.$on('showPaymentList', (data) => {
            self.pm_cc=false;
            self.pm_listing=true;
        });

    },
    methods: {

    }
});
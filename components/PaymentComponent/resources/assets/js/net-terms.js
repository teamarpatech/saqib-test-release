import '../../../vue_common/vue-config';
import '../../../vue_common/vue-validation';
import '../../../vue_common/vue-mixin';
import '../../../vue_common/vue-directive';

let netTermContainer = new Vue({
    el : '#net-term-container',
    data : {

        root_url : root_url,

        // show messages
        showMessage : false,
        messageClass : '',
        messageText : '',

        finalMessageText : '',

        div : {
            parentContainer : false,

            businessAccordionCloseShow : false,
            businessAccordionOpenShow : true,

            billingAccordionCloseShow : true,
            billingAccordionOpenShow : false,
        },

        formData : {

            business : {
                organization_name : '',
                phone : '',
                address_line_1 : '',
                address_line_2 : '',
                city : '',
                state : '',
                zip_code : '',
                duns_number : '',
                federal_id_number : '',
                type : 'company',
            },

            billing : {
                copyFormBusiness : '',
                organization_name : '',
                phone : '',
                address_line_1 : '',
                address_line_2 : '',
                city : '',
                state : '',
                zip_code : '',
                type : 'billing',
                submitButton: true
            }
        },
    },

    mounted() {
        this.div.parentContainer = true;
    },

    methods:{

        showBusinessForm : function () {

            this.showMessage = false;

            this.div.businessAccordionCloseShow = false;
            this.div.businessAccordionOpenShow = true;

            this.div.billingAccordionCloseShow = true;
            this.div.billingAccordionOpenShow = false;
        },

        showBillingForm : function () {

            this.showMessage = false;

            this.div.businessAccordionCloseShow = true;
            this.div.businessAccordionOpenShow = false;

            this.div.billingAccordionCloseShow = false;
            this.div.billingAccordionOpenShow = true;
        },

        submitBusinessForm : function () {
            this.showMessage = false;
            this.businessValidate();
        },

        businessValidate : function () {

            this.$validator.validateAll('businessForm').then((result) => {

                if(result) {
                    this.sendBusinessRequest();
                }
                else {
                    this.handleMessage(true, 0, this.errorBag.all()[0]);
                }

            });
        },

        sendBusinessRequest : function () {

            this.$http.post('payments/create-net-terms', this.formData.business).then(response => response.json()).then(
                (response) => {

                    if (response.success && response.status_code == 200) {
                        this.showBillingForm();
                        this.finalMessageText = response.message;
                    }
                    else {
                        this.handleMessage(true, 0, response.message, response.status_code);
                    }

            }, (error) => {
                    console.error('payments/create-net-terms');
                    console.error(error);
            });
        },

        billingEqualsToBusiness : function () {
            this.formData.billing.organization_name = this.formData.business.organization_name;
            this.formData.billing.address_line_1 = this.formData.business.address_line_1;
            this.formData.billing.address_line_2 = this.formData.business.address_line_2;
            this.formData.billing.city = this.formData.business.city;
            this.formData.billing.state = this.formData.business.state;
            this.formData.billing.zip_code = this.formData.business.zip_code;
            this.formData.billing.phone = this.formData.business.phone;
        },

        setBillingDataEmpty : function () {
            this.formData.billing.organization_name = "";
            this.formData.billing.address_line_1 = "";
            this.formData.billing.address_line_2 = "";
            this.formData.billing.city = "";
            this.formData.billing.state = "";
            this.formData.billing.zip_code = "";
            this.formData.billing.phone = "";
        },

        // copy form business to billing
        copyFormBusiness : function () {

            this.showMessage = false;

            if(this.formData.billing.copyFormBusiness) {
                this.billingEqualsToBusiness();
            }
            else {
                this.setBillingDataEmpty();
            }
        },

        submitBillingForm : function () {
            this.showMessage = false;
            this.billingValidate();
        },

        billingValidate : function () {

            this.$validator.validateAll('billingForm').then((result) => {

                if(result) {
                    this.sendBillingRequest();
                }
                else {
                    this.handleMessage(true, 0, this.errorBag.all()[0]);
                }

            });
        },

        // send request to api for save data.
        sendBillingRequest : function () {

            if(this.formData.billing.copyFormBusiness) {
                this.billingEqualsToBusiness();
            }

            this.$http.post('payments/create-billing-address', this.formData.billing).then(response => response.json()).then(
                (response) => {

                    this.messageClass = 0;
                    this.messageText = response.message;

                    if (response.success && response.status_code == 200) {

                        this.setBillingDataEmpty();
                        this.messageClass = 1;
                        this.messageText = this.finalMessageText;
                        this.formData.billing.submitButton = false;

                        $("#billing-copyFormBusiness").attr("disabled", true);
                    }

                    this.handleMessage(true, this.messageClass, this.messageText, response.status_code);

            }, (error) => {
                    console.error('payments/create-billing-address');
                    console.error(error);
            });

        },

        redirectToPaymentPage : function () {
            window.location.href = this.root_url+"/payments"
        }

    },
});
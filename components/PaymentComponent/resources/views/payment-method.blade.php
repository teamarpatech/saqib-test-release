@extends('layouts.new_master_shop')

@push('styles')
    <link href="{{ mix('components/payment/css/payment-method.css') }}" rel="stylesheet">

@endpush

@section('page_title', 'Payment Method')
@section('content_shop')




    <section class="container my-profile clearfix">


    @include('partials.account.left_side_bar')

    <!-- Payment Method Page Starts Here -->
        <section class="main-content container" id="PaymentApp">
            <payment-method-listing   v-if="pm_listing" :root_url="root_url" ></payment-method-listing>
            <payment-method-credit-card  v-if="pm_cc" :root_url="root_url" :drop_down_condition="drop_down_condition" ></payment-method-credit-card>
    </section>
    </section>








    </div>





@endsection


@push('scripts')
    <script src="{{ asset('components/payment/js/payment-method.js') }}"></script>
@endpush



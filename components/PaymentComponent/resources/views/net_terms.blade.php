@extends('layouts.new_master_shop')

@push('styles')
    <link href="{{ mix('components/ux/css/payment-method.css') }}" rel="stylesheet">
    <link href="{{ mix('components/payment/css/net-terms.css') }}" rel="stylesheet">
@endpush

@section('page_title', 'Net Terms')
@section('content_shop')

    <section class="container my-profile clearfix net-term-container">

        @include('partials.account.left_side_bar')

        <!-- Net Terms Page Starts Here -->
        <section class="main-content container" id="net-term-container">

                <!-- Net Terms Details Starts Here -->
                <div v-cloak class="all-payment-methods">

                    <!-- Page Heading Starts Here -->
                    <div class="page-heading">
                        <span>Net Terms Request</span>
                    </div>
                    <!-- Page Heading Ends Here -->

                    <!-- Heading Slogan Starts Here -->
                    <span class="form-slogan">
                        To request Net Terms for CCP, please provide the following information (all fields required) and submit.
                    </span>
                    <!-- Heading Slogan Ends Here -->

                    <div v-if="div.parentContainer" class="parent-container">

                        <div v-if="div.businessAccordionCloseShow" class="detail-box ccp-form-step">
                            <span class="step-title"> Business Information</span>
                        </div>

                        <div class="clearfix"></div>

                        <div v-if="div.businessAccordionOpenShow" class="ccp-form-step increment-num">
                            <span class="step-title"> Business Information </span>
                            <br><br>

                            <form action="" onsubmit="return false" data-vv-scope="businessForm">
                                <div class="form-horizontal">
                                    <fieldset class="input-field-set">

                                        <div class="form-group">
                                            <label for="business-name" class="col-sm-2 col-xs-12 form-label">
                                                <span class="v-center-lbl">
                                                    Business Name
                                                    <span class="asterik">*</span>
                                                </span>
                                            </label>

                                            <div class="col-sm-4">
                                                <input type="text" name="business-name" id="business-name" v-model="formData.business.organization_name" :class="{ 'error-field': checkError('business-name') }" class="form-control ccp-txtbox ml-1" v-validate="'required|max:50'" data-vv-as="Business Name" />
                                            </div>

                                            <label for="business-phone" class="col-sm-2 col-xs-12 form-label mobile-padding">
                                                <span class="v-center-lbl">
                                                    Business Phone
                                                    <span class="asterik">*</span>
                                                </span>
                                            </label>

                                            <div class="col-sm-4">
                                                <input type="text" name="business-phone" id="business-phone" v-model="formData.business.phone" :class="{ 'error-field': checkError('business-phone') }" class="form-control ccp-txtbox ml-1" v-validate="'required|cPhone'" data-vv-as="Business Phone" v-mask="'###.###.####'" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="business-address" class="col-sm-2 col-xs-12 form-label">
                                                <span class="v-center-lbl">
                                                    Address Line 1
                                                    <span class="asterik">*</span>
                                                </span>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="business-address" id="business-address" v-model="formData.business.address_line_1" :class="{ 'error-field': checkError('business-address') }" class="form-control ccp-txtbox ml-1" v-validate="'required'" data-vv-as="Address Line 1" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="business-address2" class="col-sm-2 col-xs-12 form-label">
                                                <span class="v-center-lbl">
                                                    Address Line 2
                                                </span>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="business-address2" id="business-address2" v-model="formData.business.address_line_2" class="form-control ccp-txtbox ml-1" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="business-city" class="col-sm-2 col-xs-12 form-label">
                                                <span class="v-center-lbl">
                                                    City
                                                    <span class="asterik">*</span>
                                                </span>
                                            </label>

                                            <div class="col-sm-4">
                                                <input type="text" name="business-city" id="business-city" v-model="formData.business.city" :class="{ 'error-field': checkError('business-city') }" class="form-control ccp-txtbox ml-1" v-validate="'required'" data-vv-as="City" />
                                            </div>

                                            <label for="business-state" class="col-sm-2 col-xs-12 form-label mobile-padding">
                                                <span class="v-center-lbl">
                                                    State
                                                    <span class="asterik">*</span>
                                                </span>
                                            </label>

                                            <div class="col-sm-4">
                                               <select name="business-state" id="business-state" v-model="formData.business.state" :class="{ 'error-field': checkError('business-state') }" class="form-control ccp-txtbox" v-validate="'required'"  data-vv-as="State" />
                                                    <option value="">Select an Option</option>
                                                       @foreach ($all_states as $state)
                                                            <option value="{{ $state['id'] }}">{{ $state['name'] }}</option>
                                                       @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="business-zip" class="col-sm-2 col-xs-12 form-label">
                                                <span class="v-center-lbl">
                                                    Zip Code
                                                    <span class="asterik">*</span>
                                                </span>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="business-zip" id="business-zip" v-model="formData.business.zip_code" :class="{ 'error-field': checkError('business-zip') }" class="form-control ccp-txtbox ml-1" v-validate="'required|cZip'" data-vv-as="Zip Code" v-mask="'##### ####'"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="business-duns-number" class="col-sm-2 col-xs-12 form-label">
                                                <span class="v-center-lbl">
                                                    DUNS Number
                                                    <span class="asterik">*</span>
                                                </span>
                                            </label>

                                            <div class="col-sm-4">
                                                <input type="text" name="business-duns-number" id="business-duns-number" v-model="formData.business.duns_number" :class="{ 'error-field': checkError('business-duns-number') }" class="form-control ccp-txtbox ml-1" v-validate="'required|min:9'" data-vv-as="DUNS Number" v-mask="'#########'" />
                                            </div>

                                            <label for="business-federal-id-number" class="col-sm-2 col-xs-12 form-label mobile-padding">
                                                <span class="v-center-lbl">
                                                    Federal ID Number
                                                </span>
                                            </label>

                                            <div class="col-sm-4">
                                                <input type="text" name="business-federal-id-number" id="business-federal-id-number" v-model="formData.business.federal_id_number" class="form-control ccp-txtbox ml-1" v-validate="'min:9'"  v-mask="'#########'" data-vv-as="Federal ID Number"/>
                                            </div>
                                        </div>

                                        <div class="form-group" v-if="showMessage">
                                            <div class="col-md-12 col-sm-12">
                                                <alert :root_url="root_url" :message_class="messageClass" :message_text="messageText" :show_message="showMessage"></alert>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <br>
                                            <div class="col-md-12 col-sm-12 col-xs-12 pl-0-md">
                                                {{--<button class="btn-primary-lg" @click="submitBusinessForm()">Submit Request</button>--}}


                                                <a @click="submitBusinessForm()" class="btn btn-primary">Submit Request</a>
                                                <a @click="redirectToPaymentPage()" class="btn">Cancel</a>

                                                {{--<button class="btn-user-bordered-lg" @click="redirectToPaymentPage()">Cancel</button>--}}

                                            </div>
                                        </div>

                                    </fieldset>
                                </div>
                            </form>
                        </div>

                        <div class="clearfix"></div>

                        <div v-if="div.billingAccordionCloseShow" class="ccp-form-step increment-num">
                            <span class="step-title"> Billing Address </span>
                        </div>

                        <div class="clearfix"></div>

                        <div v-if="div.billingAccordionOpenShow" class="increment-num"></div>
                        <div v-if="div.billingAccordionOpenShow" data-vv-scope="billingForm" class="ccp-form-step increment-num">
                            <span class="step-title"> Billing Address </span>

                            <form action="" onsubmit="return false" data-vv-scope="billingForm">
                                <div class="form-horizontal">
                                    <fieldset class="input-field-set">

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <span class="checkbox checkbox-primary">
                                                    <input id="billing-copyFormBusiness" @change="copyFormBusiness" value="1" name="billing-copyFormBusiness" v-model="formData.billing.copyFormBusiness" type="checkbox" class="custom-control-input">
                                                    <label for="billing-copyFormBusiness">My billing information is the same as business information</label>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="billing-organization" class="col-sm-2 col-xs-12 form-label form-label">
                                                <span class="v-center-lbl">
                                                    Organization Name
                                                    <span class="asterik">*</span>
                                                </span>
                                            </label>
                                            <div class="col-sm-10">
                                                <input :disabled="formData.billing.copyFormBusiness == 1" name="billing-organization" id="billing-organization" :class="{ 'error-field': checkError('billing-organization') }" type="text" v-model="formData.billing.organization_name" class="form-control ccp-txtbox ml-1" v-validate="'required'" data-vv-as="Organization Name" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="billing-address" class="col-sm-2 col-xs-12 form-label">
                                                <span class="v-center-lbl">
                                                    Address Line 1
                                                    <span class="asterik">*</span>
                                                </span>
                                            </label>
                                            <div class="col-sm-10">
                                                <input :disabled="formData.billing.copyFormBusiness == 1" name="billing-address" id="billing-address" :class="{ 'error-field': checkError('billing-address') }" type="text" v-model="formData.billing.address_line_1" class="form-control ccp-txtbox ml-1" v-validate="'required'" data-vv-as="Address Line 1" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="billing-address2" class="col-sm-2 col-xs-12 form-label">
                                                <span class="v-center-lbl">
                                                    Address Line 2
                                                </span>
                                            </label>
                                            <div class="col-sm-10">
                                                <input :disabled="formData.billing.copyFormBusiness == 1" name="billing-address2" id="billing-address2" type="text" v-model="formData.billing.address_line_2" class="form-control ccp-txtbox ml-1" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="billing-city" class="col-sm-2 col-xs-12 form-label">
                                                <span class="v-center-lbl">
                                                    City
                                                    <span class="asterik">*</span>
                                                </span>
                                            </label>

                                            <div class="col-sm-4">
                                                <input :disabled="formData.billing.copyFormBusiness == 1" name="billing-city" id="billing-city" :class="{ 'error-field': checkError('billing-city') }" type="text" v-model="formData.billing.city" class="form-control ccp-txtbox ml-1" v-validate="'required'" data-vv-as="City" />
                                            </div>

                                            <label for="billing-state" class="col-sm-2 col-xs-12 form-label mobile-padding">
                                                <span class="v-center-lbl">
                                                    State
                                                    <span class="asterik">*</span>
                                                </span>
                                            </label>

                                            <div class="col-sm-4">
                                                <select :disabled="formData.billing.copyFormBusiness == 1" name="billing-state" id="billing-state" :class="{ 'error-field': checkError('billing-state') }" v-model="formData.billing.state" class="form-control ccp-txtbox" aria-required="true" aria-invalid="false" v-validate="'required'" data-vv-as="State" >
                                                    <option value="">Select an Option</option>
                                                    @foreach ($all_states as $state)
                                                         <option value="{{ $state['id'] }}">{{ $state['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="billing-zip" class="col-sm-2 col-xs-12 form-label">
                                                <span class="v-center-lbl">
                                                    Zip Code
                                                    <span class="asterik">*</span>
                                                </span>
                                            </label>
                                            <div class="col-sm-10">
                                                <input :disabled="formData.billing.copyFormBusiness == 1" name="billing-zip" id="billing-zip" :class="{ 'error-field': checkError('billing-zip') }" type="text" v-model="formData.billing.zip_code" class="form-control ccp-txtbox ml-1" v-validate="'required|cZip'" data-vv-as="Zip Code" v-mask="'##### ####'"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="billing-phone" class="col-sm-2 col-xs-12 form-label">
                                                <span class="v-center-lbl">
                                                    Phone
                                                    <span class="asterik">*</span>
                                                </span>
                                            </label>
                                            <div class="col-sm-10">
                                                <input :disabled="formData.billing.copyFormBusiness == 1" name="billing-phone" id="billing-phone" :class="{ 'error-field': checkError('billing-phone') }" type="text" v-model="formData.billing.phone" class="form-control ccp-txtbox ml-1" v-validate="'required|cPhone'" data-vv-as="Phone"v-mask="'###.###.####'" />
                                            </div>
                                        </div>

                                        <div class="form-group" v-if="showMessage">
                                            <div class="col-md-12 col-sm-12">
                                                <alert :root_url="root_url" :message_class="messageClass" :message_text="messageText" :show_message="showMessage"></alert>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <button class="btn-primary-lg" :disabled="!formData.billing.submitButton" @click="submitBillingForm()">Submit Request</button>
                                                <button class="btn-user-bordered-lg" @click="redirectToPaymentPage()">Cancel</button>
                                            </div>
                                        </div>

                                    </fieldset>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <!-- Net Terms Page Ends Here -->

            </section>
    </section>

@endsection


@push('scripts')
    <script type="text/javascript" src="{{ mix('components/payment/js/net-terms.js') }}"></script>
@endpush



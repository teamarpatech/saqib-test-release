<?php

namespace Component\PaymentComponent\App\Providers;

use Illuminate\Support\ServiceProvider;
use Route;

class PaymentComponentServiceProvider extends ServiceProvider {


    protected $ComponentName;
    protected $path;

    public function __construct($app)
    {
        parent::__construct($app);

        $this->name = 'Payment';
        $this->ComponentName = $this->name.'Component';
        $this->path = __DIR__ . '/../../';
    }

   /**
    * Bootstrap the application services.
    *
    * @return void
    */
   public function boot()
   {
        $this->loadViewsFrom($this->path . '/resources/views', $this->ComponentName);

        // publish components assets
       $this->publishes([
           $this->path . 'resources/assets' => resource_path('assets/components/' . strtolower($this->name)),
       ], $this->ComponentName);
   }

   /**
    * Register the application services.
    *
    * @return void
    */
   public function register()
   {
      //Register Our Package routes
      Route::group([
		'namespace' => 'Component\\'.$this->ComponentName.'\App\Http\Controllers',
		'prefix' => 'payments',
		'as' => $this->ComponentName.'.',
		'middleware'=> ['web']
      ], function ($router) {
          include $this->path.'routes/routes.php';
      });

      // Component local configuration
      $this->mergeConfigFrom(
        $this->path.'config/'.$this->ComponentName.'.php', $this->ComponentName
      );
  }

}
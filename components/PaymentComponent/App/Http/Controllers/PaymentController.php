<?php

namespace Component\PaymentComponent\App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class PaymentController extends Controller
{


    public function __construct()
    {

    }

    public static function index()
    {
        return view('PaymentComponent::payment-method');
    }

    /**
     * get credit cards and net term details
     * @return array
     */
    public function getPaymentMethod(Request $request)
    {
        $response = [];
        $response['credit_card'] = $this->getCreditCardDetail($request);
        $response['billing_address'] = $this->getAdressByType();
        $response['net_term'] = $this->getActiveNetTerm($request);
        return $response;
    }

    /**
     * get credit cards and net term details
     * @return array
     */
    public function getCreditCardDetail(Request $request)
    {
        $param['company_id'] = \AuthHandler::company_id();
        return filter_response(handleResponse(GetAPI('payment', 'GET', 'getCreditCardDetail', $param)));
    }

    /**
     * get credit cards and net term details
     * @return array
     */
    public function getAdressByType()
    {
        $param['company_id'] = \AuthHandler::company_id();
        $param['type'] = 'billing';
        return filter_response(handleResponse(GetAPI('account', 'GET', 'getAddressByType', $param)));
    }

    /**
     * get net term detail
     * @return array
     */
    public function getNetTermDetail()
    {
        $param['account_id'] = \AuthHandler::company_id();
        return filter_response(handleResponse(GetAPI('payment', 'GET', 'getNetTermDetail', $param)));


    }


    // Fetch all states from API server.
    function getStates()
    {
        return filterResponse(handleResponse(GetAPI('account', 'GET', 'getStates', [])));
    }

    /**
     * create credit card address
     * @return mixed
     */
    public function createCreditCard(Request $request)
    {
        $response = [];
        $paramCreateAddress = $request->only(['organization_name', 'address_line_1', 'address_line_2', 'city', 'state', 'zip_code', 'phone', 'ext']);
        $paramCreditCard = $request->only(['name', 'type', 'number', 'cvv', 'month', 'year']);
        $company_id = \AuthHandler::company_id();
        $paramCreateAddress['account_id'] = $company_id;
        $paramCreateAddress['type'] = 'billing';
        $paramCreateAddress['email'] = \AuthHandler::email();
        $paramCreditCard['company_id'] = $company_id;
        $response['createCreditCardResponse'] = filterResponse(handleResponse(GetAPI('payment', 'POST', 'addCreditCard', $paramCreditCard)));
        //if credit card data submit successfully then submit billing address
        if ($response['createCreditCardResponse']['data']['error_type'] == 1) {
            $response['createAddressResponse'] = filterResponse(handleResponse(GetAPI('account', 'POST', 'addAddress', $paramCreateAddress)));
        }
        return $response;
    }


    /**
     * logic for showing credit card or net term or net term pending message in listing
     * @return mixed
     */
    public function getActiveNetTerm()
    {
        $record = [];
        $record['record_exist'] = false;
        $record['show_credit_card'] = true;
        //if net Term is not submitted yet
        $records = $this->getNetTermDetail();
        if (empty($records['data'])) {
            return $record;
        }
        $record['record_exist'] = true;
        $record['pending_status'] = false;
        $record['active_net_term'] = [];
        //status of last record of net term
        $lastRecord = collect($records['data'])->last();
        //if last record is active show net term detail only
        if ($lastRecord['status'] == 1) {
            $record['show_credit_card'] = false;
            $record['active_net_term'] = $lastRecord;
         //if last record is not active find if active record is present in previous records
        } elseif ($lastRecord['status'] == 0) {
            $collection = collect($records['data']);
            $filtered = $collection->where('status', 1);
            $filtered_record = $filtered->all();
            $record['pending_status'] = true;
            //if active  net term found display net term detail only
            if (!empty($filtered_record)) {
                $record['show_credit_card'] = false;
            }
            $record['active_net_term'] = $filtered_record;

        }
        return $record;

    }
}
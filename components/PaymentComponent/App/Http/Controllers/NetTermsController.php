<?php

    namespace Component\PaymentComponent\App\Http\Controllers;

    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;

    class NetTermsController extends Controller
    {
        // Create net term and billing address html page
        public function index()
        {

            $all_states = $this->getStates()['data'];
            return view('PaymentComponent::net_terms', compact('all_states'));
        }

        // Fetch all states from API server.
        public function getStates()
        {
            return filterResponse(handleResponse(GetAPI('account', 'GET', 'getStates', [])));
        }

        // Send Net Term request to API server.
        public function createNetTerm(Request $request) {

            $params['json'] = $request->only('organization_name', 'address_line_1', 'address_line_2', 'city', 'state', 'zip_code', 'phone', 'type', 'duns_number', 'federal_id_number');
            $params['json']['account_id'] = \AuthHandler::company_id();

            if($params['json']['federal_id_number'] == '') {
                unset($params['json']['federal_id_number']);
            }

            if($params['json']['address_line_2'] == '') {
                unset($params['json']['address_line_2']);
            }

            $response = filterResponse(handleResponse(GetAPI('payment', 'POST', 'createNetTerms', $params)));

            return $response;
        }

        // Send Create Billing Address request to API server.
        public function createBillingAddress(Request $request) {

            $address = $this->getAddress();

            // in case user send create request and address already created.
            if(collect($address['data'])->isNotEmpty()) {

                $address_id = $params['address_id'] = $address['data'][0]['id'];
                return $this->changeRequestBillingAddress($request, $address_id);
            }


            $params = $request->only('organization_name', 'address_line_1', 'address_line_2', 'city', 'state', 'zip_code', 'phone', 'type', 'ext');
            $params['account_id'] = \AuthHandler::company_id();
            return filterResponse(handleResponse(GetAPI('account', 'POST', 'addAddress', $params)));
        }

        // Fetch existing billing address
        function getAddress() {
            $params['type'] = 'billing';
            $params['account_id'] = \AuthHandler::company_id();

            return filterResponse(handleResponse(GetAPI('account', 'GET', 'getAddress', $params)));
        }

        // Send Update Billing Address request to API server.
        public function changeRequestBillingAddress(Request $request, $address_id) {

            $params = $request->only('organization_name', 'address_line_1', 'address_line_2', 'city', 'state', 'zip_code', 'phone', 'type', 'ext', 'address_id');
            $params['account_id'] = \AuthHandler::company_id();
            $params['address_id'] = $address_id;

            return filterResponse(handleResponse(GetAPI('account', 'POST', 'updateAddress', $params)));
        }
    }
<?php

Route::group(['middleware' => 'userpermission:payments-management',], function () {
    Route::get('/', 'PaymentController@index')->name('payment-method-listing');
    Route::get('states', 'PaymentController@getStates')->name('payment-get-states');
    Route::get('detail', 'PaymentController@getPaymentMethod')->name('payment-method-detail');
    Route::post('credit-card', 'PaymentController@createCreditCard')->name('create-credit-card');
    Route::get('/payment-detail', 'PaymentController@getPaymentMethod')->name('payment-method-detail');
    Route::get('/net-terms', 'NetTermsController@index')->name('net-terms');
    Route::post('/get-states', 'NetTermsController@getStates')->name('get-states');
    Route::post('/create-net-terms', 'NetTermsController@createNetTerm')->name('create-net-terms');
    Route::post('/create-billing-address', 'NetTermsController@createBillingAddress')->name('create-billing-address');

});


<?php
/**
 * Created by PhpStorm.
 * User: faizan.sarfaraz
 * Date: 8/6/2018
 * Time: 2:22 PM
 */

namespace App\Http\Controllers;


class DashboardController
{

    public function showProfile()
    {
        return view('pages/my-profile');
    }
}
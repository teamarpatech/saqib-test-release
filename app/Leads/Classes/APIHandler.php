<?php

namespace Leads\Classes;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class APIHandler
{
    private $APIs;
    private $apiBase;
    private $authToken;

    public function __construct(){
        $this->APIs = config('apis');
        $this->apiBase = config('app.API_BASEURL');
        $this->authToken = \AuthHandler::check() ? \AuthHandler::authToken() : NULL;
    }

    /**
     * Get result for specific API
     * @param  string $group    API key for config
     * @param  array  $data     Parametrs and values
     * @param  string $method
     * @param  int $index
     * @return json output
     */
    public function get($group, $method = 'GET', $index = 0, $data = [], $authToken = NULL)
    {

        $api = $this->APIs[$group][$method][$index];

        // Use header token or not
        $useToken = isset($api['use_token']) ? $api['use_token'] : true;

        // Final end point
        $endPoint = $api['end_point'];

        // API required headers
        $apiHeaders = isset($api['headers']) ? $api['headers'] : [];

        // Replace URL parameters with data
        $endPoint = $this->handleURLParameters($endPoint, $data);

        try{

            // Direct call to API if cache is false
            return $this->callApi($endPoint, $data, $method, $useToken, $apiHeaders, $authToken);

        }catch(\Exception $ex){
            dd($ex->getMessage());
        }


    }

    /**
     * Get result for specific API from endpoint
     * @param  string $endPoint
     * @param  array  $params
     * @return json output
     */
    private function callApi($endPoint, $params = [], $method, $useToken = true, $apiHeaders = [], $authToken = NULL)
    {
        $headers = [];

        if ($useToken) {
            $headers['headers'] = [
                'Authorization' => 'Bearer ' . (isset($authToken) ? $authToken : $this->authToken)
            ];
        }
        $headers['http_errors'] = false;

        $headers['base_uri'] = $this->apiBase;

        // $headers['debug'] = true;

        $params['portal_type'] = 'customer';

        if(isset($params['json'])){
            $headers['json'] = $params['json'];
            $headers['json']['portal_type'] = 'customer';
        }else{
            $headers[$method == 'POST' ? 'form_params' : 'query'] = $params;
        }

        // Include required headers for the API
        $headers = array_merge_recursive($headers,$apiHeaders);

        $client = new Client();

        $response = $client->request($method, $endPoint, $headers);

        $hasJson = strpos($response->getHeaderLine('content-type'), 'json');

        // Data to send to view
        $data = [];
        $data['statusCode'] = $response->getStatusCode();
        $data['contentType'] = $response->getHeaderLine('content-type');


        $data['content'] = $response->getStatusCode() == 204 ? json_encode([]) : $response->getBody()->getContents();
        // Decode if has json header
        if ($hasJson){
            $data['response'] = \GuzzleHttp\json_decode($data['content']);
        }
        if ($response->getStatusCode() != 200 && $hasJson) {
            $body = json_decode($data['content'], true);
            $data += $body;
        }
        return $data;
    }


    /**
     * Replace any variable included in the end point
     * @param  string $endpoint
     * @param  array $data
     * @return string
     */
    public function handleURLParameters($endPoint, $data)
    {
        $endPoint = $this->removeFilter($endPoint);

        return preg_replace_callback('/(\{.*?\})/',
            function($matches) use ($data) {
                $key = trim($matches[1],"{}");
                $pram = isset($data[$key]) ? $data[$key] : $matches[1];
                return ($key == 'mfgName')?$pram:urlencode($pram);
            },
            $endPoint
        );
    }

    /**
     * Remove Subfilter :filter
     * @param  string $endPoint
     * @return string valid endpoint for gyro
     */
    function removeFilter($endPoint)
    {
        $pos = strrpos($endPoint, '::');

        // Check if has filter
        if($pos !== false)
        {
            // Get string before colon
            $endPoint = strstr($endPoint, '::', true);
        }

        return $endPoint;
    }


}

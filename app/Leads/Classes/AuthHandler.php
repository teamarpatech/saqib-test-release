<?php

namespace Leads\Classes;

use Illuminate\Http\Request;
use Session;

class AuthHandler
{
    /**
     * Login using auth token
     */
    public function login(Request $request)
    {
        $authToken = $request->input('auth_token');
        $params = [
            'authToken' => $authToken
        ];
        // Get user information from API
        $result = GetAPI('users-resource','GET' ,'users', $params, $authToken);
        $response = handleResponse($result);
        $sessionData = [];
        $sessionData['authToken'] = $authToken;
        $sessionData['sessionStartTime'] = time();
        $sessionData['rights'] = [];
        // Include user data in session
        $sessionData += collect($response['response']->data)->toArray();


        // Get user permissions information from API
        $permission_result = GetAPI('users-resource', 'GET', 'permissions', ['userId' => $sessionData['id']],$authToken);
        if ($permission_result['statusCode'] == 200) {
            $sessionData['rights'] = $permission_result['response']->data;
        }
        Session::put('users-resource', $sessionData);
        // Get user redirect to link after login
        $homePageUrl = $this->getHomePageRoute();
        // Check if redirect to is present in query string
        $redirectTo = $request->input('redirect_to', $homePageUrl);
        return redirect($redirectTo);
    }

    function getHomePageRoute(){
        
        $homePageUrl = route(config('main.dashboard_route'));
        $sessionData = Session::get('users-resource');
        if (isset($sessionData['homePageUrl'])){
            switch ($sessionData['homePageUrl']) {
                case '/app/MainPage':
                    $homePageUrl = route(config('main.dashboard_route'));
                    break;
                case '/app/Track':
                    $homePageUrl = route('track.dashboard');
                    break;
                case '/cbs_order_asp/StandardDisplayCartX.php':
                    $homePageUrl = route('shop.standards');
                    break;
            }
        }
        return $homePageUrl;
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {        
        $request->session()->invalidate();

        return redirect()->to(config('main.login_page'));
    }

    /**
     * Get session status
     */
    public function check()
    {
        return \Session::has('users-resource');
    }

    /**
     * Expose magic methods
     */
    public function __call($method, $param = [])
    {
    	$session = Session::get('users-resource', []);
        return isset($session[$method]) ? $session[$method] : null;
    }

    function fullName(){
        return $this->first_name() . ' ' . $this->last_name();
    }

    // get latest data from database after update profile
    function nameInitials(){
        return strtoupper($this->first_name()[0] . $this->last_name()[0]);
    }

    /**
     * check user can access resource
     * @param [string]  permission
     * @return  [boolean] user can access or not
     */
    function can($permission)
    {
        $session = Session::get('users-resource');
        $rolePermissions = isset($session['rights']) ? $session['rights'] : [];

        //we can use this code when we do not get permission list from our api level
        //permission list for admin for testing purpose
        $permissions_admin=  ['user-management','catalog-management','address-management','settings-management','payments-management','domain-management','password-management','profile-management'];
        //permission list for admin for testing purpose
        $permissions_user=  ['password-management','profile-management'];
        if($session['rights'] !='' && $session['rights'][0]->name=='Admin'){
            $permissions=$permissions_admin;
        }else{
            $permissions=$permissions_user;

        }
        //we can use this code when we get permission list from our api level
    /*   $permissions = collect($rolePermissions)->map(function($item){
            return $item->permissions;
        })->flatten()->toArray();*/
        return in_array($permission,$permissions);
    }

    function infoUpdateInSession($param) {

        foreach($param as $key => $array) {
            Session::put('users-resource.'.$key, $array);
        }

    }
}

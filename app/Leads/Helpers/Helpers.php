<?php

    function GetAPI($group, $method, $index, $data, $authToken = NULL){

        return APIHandler::get($group, $method, $index, $data, $authToken);
    }

    function handleResponse($response, $callback = NULL){

        return $response;
    }
    // helper function user for user can access resource or not
    function userCan($permission)
    {
        return AuthHandler::can($permission);
    }


    function sendEmailWithTemplate($params, $replacementOpen = "{{", $replacementClose = "}}")
    {
        if(empty($params['to']['email'])) {
            return "Required Email.";
        }

        if(empty($params['subject']['title'])) {
            return "Required Email Subject.";
        }

        if(empty($params['template']['id'])) {
            return "Required Template ID.";
        }

        Mail::send([], [], function ($mail) use ($params, $replacementOpen, $replacementClose) {

            $subject = (isset($params['subject']['title'])) ?  $params['subject']['title'] : "";

            $to_name = (isset($params['to']['name'])) ? $params['to']['name'] : "";
            $to_email = (isset($params['to']['email'])) ? $params['to']['email'] : "";

            $from_name = (isset($params['from']['name'])) ? $params['from']['name'] : "";
            $from_email = (isset($params['from']['email'])) ? $params['from']['email'] : config('constants.from_email');

            $template_id = (isset($params['template']['id'])) ? $params['template']['id'] : "";


            foreach ($params['template_data'] as $key => $val)
            {
                $data['sub'][$replacementOpen.$key.$replacementClose] = ["$val"];
            }

            $data['filters']['templates']['settings']['enable'] = 1;
            $data['filters']['templates']['settings']['template_id'] = $template_id;

            $mail->subject($subject);
            $mail->to($to_email, $to_name);
            $mail->from($from_email, $from_name);
            $mail->setContentType('text/html');
            $mail->getSwiftMessage()->getHeaders()->addTextHeader('X-SMTPAPI', asString($data));

        });

        return true;
    }

    function asJSON($data)
    {
        $json = json_encode($data);
        $json = preg_replace('/(["\]}])([,:])(["\[{])/', '$1$2 $3', $json);
        return $json;
    }

    function asString($data)
    {
        $json = asJSON($data);
        return wordwrap($json, 76, "\n   ");
    }

    /**
     * filtering response by encoding
     */
    if (! function_exists('filterResponse')) {
    	function filterResponse($response) {
    		return json_decode(json_encode(optional($response)['response']), true);
    	}
    }

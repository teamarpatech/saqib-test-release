<?php

namespace Leads\Middlewares;
use Closure;

class UserPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next,$permission)
    {
        if (!userCan($permission)) {
            return abort(403, 'Unauthorized action');
        }
        return $next($request);
    }
}
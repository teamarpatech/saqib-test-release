<?php

namespace Leads\Middlewares;

use Closure;
use AuthHandler;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (AuthHandler::check()) {
            return redirect()->route(config('main.dashboard_route'));
        }

        return $next($request);
    }
}

<?php

namespace Leads\Middlewares;

use Closure;
use Illuminate\Auth\AuthenticationException;

class Authenticate
{

    protected $except_urls = [
		'account/create-account',
		'post-signup-step1',
		'post-send-email',
		'verify-email/[a-zA-Z0-9_]+/[a-zA-Z0-9_]+',
		'verify-invite/[a-zA-Z0-9_]+',
		'post-verify-code',
		'get-industry-types',
		'get-comapny-sizes',
		'add-business-information',
		'get-job-titles',
		'add-user-information',
		'get-subscription-plans',
		'update-subscription-plan',
		'get-license-agreement',
		'update-company-agreement',
		'post-request-access-email',
		'auth/login',
		'auth/authenticate',
		'auth/logout',
		'auth/forgot-password',
		'auth/reset-password',
		'get-states',
		'post-create-address',
        'get-states',
        'post-create-address',
        'signup-invited-user',
    ];

    public function __construct()
    {
        $this->timeout = config('LoginComponent.login_session','session.lifetime')  * 60;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $regex = '#' . implode('|', $this->except_urls) . '#';
        // not checking excluded path and checking if there user resource exist then check
        if (!preg_match($regex, $request->path()) && !\AuthHandler::check()) {
            if($request->wantsJson()){
                throw new AuthenticationException('Unauthenticated.');
            }
            $from = '';
            if($request->path() != '/'){
                $from = '?redirect_to='.$request->fullUrl();
            }
            \AuthHandler::logout($request);
            return redirect()->to(config('main.login_page').$from);
        }
        // remaining session time and logout user and redirect to login page,
        // if user resource in session
        if (\AuthHandler::check()) {
            if (time() - \AuthHandler::sessionStartTime() > $this->timeout){
                $from = '';
                if($request->path() != '/')
                    $from = '?redirect_to='.$request->fullUrl();

                \AuthHandler::logout($request);
                return redirect()->to(config('main.login_page').$from);
            }
        }
        return $next($request);
    }

}

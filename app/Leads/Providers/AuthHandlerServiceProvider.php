<?php

namespace Leads\Providers;

use Illuminate\Support\ServiceProvider;

class AuthHandlerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('AuthHandler',function() {
            return new \Leads\Classes\AuthHandler;
        });
    }
}
